<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model 
{

    protected $table = 'teachers';
    public $timestamps = true;
    protected $fillable = ['name'];

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

}