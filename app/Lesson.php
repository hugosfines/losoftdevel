<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model 
{

    protected $table = 'class';
    public $timestamps = true;

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function attendance()
    {
        return $this->hasOne(Attendance::class);
    }

    public function getDayHourAttribute(){
        setlocale(LC_TIME, "es_ES");

        $date = new \DateTime($this->datetime);
        $day = $date->format('l');  
        
        $schedule = $this->group->schedules->where('week', $this->week)->where('day_week',$date->format('w'));
        $resp = "";
        
        if($schedule->count() > 0 ){
            $schedule = $schedule->first();
            $resp = "$day $schedule->init_time - $schedule->end_time";
        }
        return $resp;
    }

}