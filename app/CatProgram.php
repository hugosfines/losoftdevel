<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatProgram extends Model 
{

    protected $table = 'cat_program';
    public $timestamps = true;

}