<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model 
{

    protected $table = 'cats';
    public $timestamps = true;
    protected $fillable = ['name', 'activo'];

    public function programs()
    {
        return $this->belongsToMany(Program::class);
    }

    public function calssrooms()
    {
        return $this->hasMany(Classroom::class);
    }

    public function weekdays(){
        return $this->hasMany(CatWeekday::class);
    }

    public function weekdayStatus($day){
        foreach ($this->weekdays as $key => $weekday) {
            if($weekday->weekday == $day and $weekday->status)
                return true;
        }
        return false;
    }

}