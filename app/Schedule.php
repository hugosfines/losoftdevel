<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';
    public $timestamps = true;
    protected $fillable = ['group_id','week', 'day_week','init_time', 'end_time'];

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
