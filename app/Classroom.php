<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model 
{

    protected $table = 'classroom';
    public $timestamps = true;
    protected $fillable = ['cat_id', 'name'];

    public function cat()
    {
        return $this->belongsTo(Cat::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

}