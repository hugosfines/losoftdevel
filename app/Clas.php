<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clas extends Model
{
   protected $table = 'class';
    public $timestamps = true;
    protected $fillable = ['group_id', 'name', 'week', 'tutorial', 'datatime'];

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
