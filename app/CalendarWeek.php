<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarWeek extends Model 
{

    protected $table = 'calendar_weeks';
    public $timestamps = true;
    protected $fillable = ['nweek', 'week', 'init_date', 'end_date'];

}