<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model 
{

    protected $table = 'programs';
    public $timestamps = true;
    protected $fillable = ['name'];
    public function cours()
    {
        return $this->belongsToMany(Cour::class);
    }

    public function cats()
    {
        return $this->belongsToMany(Cat::class);
    }

}