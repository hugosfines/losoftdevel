<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourProgram extends Model 
{

    protected $table = 'cour_program';
    public $timestamps = true;

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function cour()
    {
        return $this->belongsTo(Cour::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }


}