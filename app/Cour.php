<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cour extends Model 
{

    protected $table = 'cours';
    public $timestamps = true;
    protected $fillable = ['name'];
    public function programs()
    {
        return $this->belongsToMany(Program::class);
    }
    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }
}