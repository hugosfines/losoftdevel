<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Group;
use Illuminate\Http\Request;

class ClassController extends Controller
{
     public function index()
    {
        $classes = Lesson::with('group')->paginate(10);
        return view('classe.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = Group::get();
        $group= $group->pluck('name', 'id');
        return view('classe.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $classe = Lesson::create([
        	'group_id'=>$request->group_id,
            'name' => $request->name,
            'week' => $request->week,
            'tutorial'=>$request->tutorial,
            'datetime' =>$request->datetime,
        ]);

        return redirect()->route('classe.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classe = Lesson::find($id);
        $group = Group::get();
        $group= $group->pluck('name', 'id');

        return view('classe.edit',compact('classe','group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $classe = Lesson::find($id);
            $classe->group_id = $request->group_id;
            $classe->name = $request->name;
            $classe->week = $request->week;
            $classe->tutorial=$request->tutorial;
            $classe->datetime =$request->datetime;

        $classe->save();

        return redirect()->route('classe.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
