<?php

namespace App\Http\Controllers;

use App\WeekSchedul;
use App\Group;
use Illuminate\Http\Request;

class WeekSchedulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $weekschedul = WeekSchedul::with('group')->paginate(10);
        return view(' weekschedul.index', compact('WeekSchedul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = Group::get();
        $group= $group->pluck('name', 'id');
        return view('weekschedul.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $weekschedul = WeekSchedul::create([
            'group_id'=>$request->group_id,
            'week' => $request->week,
            'date'=>$request->date,
            'hour' =>$request->hour,
        ]);

        return redirect()->route('weekschedul.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeekSchedul  $weekSchedul
     * @return \Illuminate\Http\Response
     */
    public function show(WeekSchedul $weekSchedul)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeekSchedul  $weekSchedul
     * @return \Illuminate\Http\Response
     */
    public function edit(WeekSchedul $weekSchedul)
    {
        $weekschedul = WeekSchedul::find($id);
        $group = Group::get();
        $group= $group->pluck('name', 'id');

        return view('weekschedul.edit',compact('class','group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeekSchedul  $weekSchedul
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeekSchedul $weekSchedul)
    {
        $weekschedul = WeekSchedul::find($id);
            $weekschedul->group_id = $request->group_id;
            $weekschedul->date = $request->date;
            $weekschedul->week = $request->week;
            $weekschedul->hour=$request->hour;

        $weekschedul->save();

        return redirect()->route('weekschedul.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeekSchedul  $weekSchedul
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeekSchedul $weekSchedul)
    {
        //
    }
}
