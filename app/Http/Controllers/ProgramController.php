<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Cour;
use App\Cat;

class ProgramController extends Controller
{
   public function index()
    {
    $programs= Program::with('cours')->paginate(10);
        return view('programs.index', compact('programs'));
    }
    public function create()
    {
        return view('programs.create');
    }
    

  public function store(Request $request)
    {
        $program = Program::create([
            'name' => $request->name,
        ]);
        return redirect()->route('programs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program = Program::find($id);
        
        return view('programs.edit',compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program = Program::find($id);
            $program->name = $request->name;
        $program->save();

        return redirect()->route('programs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function listByCat(Request $request){
        $cat_id = $request->cat_id;
        $programs = Cat::where('id', $cat_id)->first()->programs;
        return response()->json($programs);
    }
}



