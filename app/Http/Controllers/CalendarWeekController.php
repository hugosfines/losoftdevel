<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarWeek;

class CalendarWeekController extends Controller 
{

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $calendarweeks =  CalendarWeek::orderBy('id', 'DESC')
            ->paginate(10);
        return view('calendarweek.index', compact('calendarweeks'));
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
     return view('calendarweek.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $calendarweeks = Calendarweeks::create([
            'name' => $request->name,
        ]);

        return redirect()->route('calendarweek.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $calendarweeks = calendarweeks::find($id);
        
        return view('calendarweek.edit',compact('calendarweeks'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit( $id)
  {
     $calendarweeks = calendarweek::find($id);
        
        return view('calendarweek.edit',compact('calendarweeks'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
     $calendarweeks = calendarweeks::find($id);
            $calendarweeks->name = $request->name;
        $calendarweeks->save();

        return redirect()->route('calendarweek.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }


  public function weekByDate(Request $request)
    {
      $date = $request->date;
      $weeks = CalendarWeek::where('init_date', '<=',$date)
                ->where('end_date','>=', $date)->orderBy('id', 'desc')->first();
                return $weeks;
        return response()->json($weeks);
    }
  
}

?>