<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Attendance;
use App\Classroom;
use App\Teacher;
use App\Cour;
use App\User;
use App\Cat;
use App\Program;
use App\Level;
use App\Lesson;
use App\Group;
use PDF;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Attendances = Attendance::with(
            [
                'lesson.group.cour_program.cour',
                'lesson.group.cour_program.level',
                'lesson.group.cour_program.program',
                'lesson.group.classroom',
                'lesson.group.teacher',
                'user',
                'lesson.group.classroom.cat'
            ])
            ->orderBy('id', 'DESC')
            ->paginate(10);

        return view('attendances.index', compact('Attendances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $cat = Cat::pluck('name', 'id')->prepend('Seleccione');
        $classroom = Classroom::pluck('name','id');
        $cour = Cour::pluck('name','id');
        $program = Program::pluck('name','id');
        $teacher = Teacher::pluck('name','id');
        $halls = Classroom::pluck('name', 'id');
        $users = User::orderBy('id','desc')->get();
        $users = $users->pluck('name', 'id');
        $levels = Level::pluck('name', 'id')->prepend('Seleccione');
        $times = ['7:00 pm a 10:00 pm'=>'7:00 pm a 10:00 pm','1:00 pm a 4:00 pm'=>'1:00 pm a 4:00 pm', '10:00 am a 1:00 pm'=>'10:00 am a 1:00 pm', '2:00 pm a 5:00 pm'=>'2:00 pm a 5:00 pm', '4:00 pm a 7:00 pm'=>'4:00 pm a 7:00 pm', '5:00 pm a 8:00 pm'=>'5:00 pm a 8:00 pm','7:00 am a 1:00 pm'=>'7:00 am a 1:00 pm', '7:00 am a 10:00 am'=>'7:00 am a 10:00 am'];

        // como tal no estas usando users

        // REVISAR HJR
        /*$lesson = Lesson::where('datetime', 'like', "2020-03-07 %")
                            ->where('group_id', '1')
                            ->where('week', '1')
                            ->where('tutorial', '1')->first();
        $group = Group::whereId('1')->first();
        return $group;*/

        return view('attendances.create', compact('classroom', 'users', 'program', 'cour', 'cat', 'teacher', 'levels', 'times'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lesson = Lesson::where('group_id', $request->group)
                            //->whereDate('datetime', "$request->datetime")
                            ->where('week', $request->week)
                            ->where('tutorial', $request->tutorial)->first();
        $group = Group::whereId($request->group)->first();
        $request->validate([
            'group' => 'required',
            'week'      => 'required',
            'tutorial'  => 'required',
            'students'  => 'required',
            'attended'  => 'required'
        ]);
        $message = '';
        if(is_null($lesson)){
            $message = "No existe clase programada para el grupo $group->name, tutoria $request->tutorial, semana $request->week y dia $request->datetime";
        }
        if($message != '')
            return redirect()->back()->with('fail', $message);

       $attendance = new Attendance();
       $attendance->fill([
            'students' => $request->students,
            'attended' => $request->attended,
            'class_id' => $lesson->id,
            'user_id'  => auth()->user()->id
        ]);
       $attendance->save();
        
        return redirect()->route('attendances.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {       

        $attendance = Attendance::find($id);

        $classrooms = Classroom::get();
        $classrooms = $classrooms->pluck('name', 'id', 'hour', 'day', 'week','level','group');
        $levels=Level::get();
        $levels= $levels->pluck('name', 'id');
        $users = User::get();
        $users= $users->pluck('name', 'id');

        return view('attendances.edit',compact('attendance','classrooms','users', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $attendance = Attendance::find($id);
            $attendance->classroom_id = $request->classroom_id;
            $attendance->professional_id = $request->professional_id;
            $attendance->hour = $request->hour;
            $attendance->day= $request->day;
            $attendance->week = $request->week;
            $attendance->level = $request->level;
            $attendance->group = $request->group;
            $attendance->tutorial=$request->tutorial;
            $attendance->students = $request->students;
            $attendance->attended = $request->attended;
        $attendance->save();
        return redirect()->route('attendances.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download(){
        $Attendances = Attendance::with(
            [
                'lesson.group.cour_program.cour',
                'lesson.group.cour_program.level',
                'lesson.group.cour_program.program',
                'lesson.group.classroom',
                'lesson.group.teacher',
                'user',
                'lesson.group.classroom.cat'
            ])
            ->orderBy('id', 'DESC')
            ->paginate(10);
         $pdf= PDF::loadView('attendances.pdf', compact('Attendances'));
         $pdf->setPaper('a4', 'landscape');
          return $pdf->stream();
    }
}
