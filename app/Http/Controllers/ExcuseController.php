<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;
use App\Excuse;

//Almacenamiento
use Illuminate\Support\Facades\Storage;

class ExcuseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Attendances = Attendance::with(['cat','classroom.cat','professional','excuse'])
            ->where([
                        ['professional_id', '=', auth()->id()],
                        ['attended', '=', 0],
                    ])
            ->orderBy('id', 'DESC')
            ->paginate(10);
        return view('excuses.index', compact('Attendances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($attendance)
    {

        return view('excuses.create', compact('attendance'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $path = Storage::disk('public')->putFile('excusas', $request->file('excuse_file'));

        $Excuse = new Excuse;
            $Excuse->attendance_id = $request->attendance_id;
            $Excuse->excuse_url = $path;  
        $Excuse->save();

        return redirect()->route('attendances.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $excuse = Excuse::find($id)->first();
        return Storage::disk('public')->download($excuse->excuse_url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
