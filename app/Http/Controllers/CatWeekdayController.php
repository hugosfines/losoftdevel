<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatWeekday;
use App\Cat;

class CatWeekdayController extends Controller 
{

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
       
        $cats = Cat::with('weekdays')->get();
        return view('catweekdays.index', compact('cats'));
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($cat)
    {
        $cat    = Cat::whereId($cat)->with('weekdays')->first();
        $options = [0 => 'No', 1 => 'Si'];
        return view('catweekdays.edit', compact('cat', 'options'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update($id, Request $request)
    {
        $cat    = Cat::whereId($id)->with('weekdays')->first();
        foreach ($request->all() as $key => $value) {
            if(is_bool(strpos($key,'-')) and !strpos($key, '-'))
                continue;
            $day = explode('-', $key)[1];
            $weekday = $cat->weekdays->where('weekday', $day)->first();
            if(empty($weekday)){
                $weekday = new CatWeekday;
                $weekday->cat_id = $cat->id;
                $weekday->weekday = $day;
            }
            $weekday->status = $value;
            $weekday->save();
        }
        return redirect()->route('catweekdays.index');
    }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }

  public function catWeekdaysByDate(Request $request)
  {
      $date = new \DateTime($request->date);
      $day  = $date->format('w'); 
      $day  = $day == 0 ? 7 : $day;
      $catweekdays = CatWeekday::where('weekday', $day)->where('status', 1)->with('cat')->get();
      $cats = [];
      foreach ($catweekdays as $catweekday) {
        $cats[] = $catweekday->cat;
      }

      return response()->json($cats);
  }
  
}

?>