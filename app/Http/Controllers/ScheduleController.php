<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Group;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
    {
     $schedules = Schedule::with('group')->paginate(10);
        return view('schedules.index', compact('schedules'));
    }
    public function create()
    {
    	$group = Group::get();
        $group= $group->pluck('name', 'id');
        return view('schedules.create', compact('group'));
    }    
    public function store()
    {
    	  $schedule= Schedule::create([
            'day_week' => $request->day_week,
            'week' => $request->week,
            'init_time'=>$request->init_time,
            'end_time' =>$request->end_time,
            'group_id'=>$request->group_id,
        ]);
    	  return redirect()->route('schedules.index');
    	}
     public function edit($id)
    {
        $schedule = Schedule::find($id);
        $group = Group::get();
        $group= $group->pluck('name', 'id');

        return view('schedules.edit',compact('schedule','group'));
    }
     public function update(Request $request, $id)
    {
       $schedule = Schedule::find($id);
            $schedule->day_week = $request->day_week;
            $schedule->init_time=$request->init_time;
            $schedule->week = $request->week;
            $schedule->end_time =$request->end_time;
			$schedule->group_id = $request->group_id;

        $schedule->save();

        return redirect()->route('schedules.index');
    }

}
