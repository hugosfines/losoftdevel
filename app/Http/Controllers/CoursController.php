<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\Cour;
use App\Level;
use App\Program;
use App\CourProgram;
use Illuminate\Http\Request;

class CoursController extends Controller
{
    public function index()
    {
    $cours = Cour::paginate(10);
        return view('cours.index', compact('cours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = Teacher::get();
        $teacher= $teacher->pluck('name', 'id');
        return view('cours.create', compact('teacher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cour = Cour::create([
            'teacher_id' => $request->teacher_id,
            'name' => $request->name,
        ]);
        return redirect()->route('cours.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cour = Cour::find($id);
        return view('cours.edit',compact('cour'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cour = Cour::find($id);
        return view('cours.edit',compact('cour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cour = Cour::find($id);
            $cour->name = $request->name;
        $cour->save();

        return redirect()->route('cours.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $cour = Cour::find($id)->delete();
        return back()->with('info', 'Eliminado correctamente');
    }

    public function listByProgram(Request $request){
        $program_id = $request->program_id;
        $cours = Program::where('id', $program_id)->first()->cours;
        return response()->json($cours);
    }

    // HJR
    public function listLevels(Request $request)
    {
        $levelsArray     = CourProgram::select(\DB::raw('MAX(level_id) as level_id'))
            ->where('program_id', $request->program_id)
            ->where('cour_id', $request->cour_id)
            ->groupBy('level_id')
            ->get();
        $levels = Level::select('id', 'name')->whereIn('id', $levelsArray)->get();
        return response()->json($levels);
    }
}

