<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourProgram;
use App\Classroom;
use App\Teacher;
use App\Group;

class GroupController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::paginate(10);
        return view('groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courprogram = CourProgram::get();
        $courprogram= $courprogram->pluck('name', 'id');
        $teacher= Teacher::get();
        $teacher= $teacher->pluck('name','id');
        $classroom = Classroom::get();
        $classroom= $classroom->pluck('name', 'id');
        return view('groups.create', compact('teacher', 'classroom', 'courprogram'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = Group::create([
            'name' => $request->name,
            'cour_program_id' => $request->cour_program_id,
            'teacher_id' => $request->teacher_id,
            'classroom_id'=> $request->classroom_id,
        ]);

        return redirect()->route('groups.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        $courprogram= CourProgram::get();
        $courprogram= $courprogram->pluck('name', 'id');
        $teacher= Teacher::get();
        $teacher= $teacher->pluck('name', 'id');
        $classroom= Classroom::get();
        $classroom= $classroom->pluck('name','id');

        return view('groups.edit', compact('group', 'teacher', 'classroom', 'cour_program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::find($id);
            $group->name = $request->name;
        $group->save();

        return redirect()->route('groups.index');
    }
    
    public function groupsByCourProgram(Request $request)
    {
    	$program_id = $request->program_id;
    	$cour_id 	= $request->cour_id;
    	$level_id 	= $request->level_id;
        $groups 	= CourProgram::where('program_id', $program_id)
        				->where('level_id', $level_id)
        				->where('cour_id', $cour_id)->first()->groups;
        return response()->json($groups);
    }

    public function show($id, Request $request){
    	if($request->ajax()){
            $group = Group::with(['teacher', 'classroom'])->where('name', $id)->first();
    		//$group = Group::find($id)->with(['teacher', 'classroom'])->first();
    		return response()->json($group);
    	}


    }
}
