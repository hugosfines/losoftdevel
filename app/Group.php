<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model 
{

    protected $table = 'groups';
    public $timestamps = true;
    protected $fillable = ['name', 'cour_program_id', 'teacher_id', 'classroom_id'];
    
    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function cour_program()
    {
        return $this->belongsTo(CourProgram::class);
    }

    public function classes()
    {
        return $this->hasMany(Lesson::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

}