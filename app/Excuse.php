<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excuse extends Model 
{

    protected $table = 'excuses';
    public $timestamps = true;
    protected $fillable = ['attendance_id', 'excuse_url'];

    public function attendance()
    {
        return $this->belongsTo(Attendance::class);
    }

}