<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model 
{

    protected $table = 'attendances';
    public $timestamps = true;

    protected $fillable = ['students', 'attended', 'class_id', 'user_id'];

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'class_id');
    }

    public function excuses()
    {
        return $this->hasMany(Excuse::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getWasAttendedAttribute(){
        return ($this->attended) ? 'Si' : 'No';
    }

}