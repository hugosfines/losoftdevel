<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatWeekday extends Model 
{

    protected $table = 'cat_weekdays';
    public $timestamps = true;
    protected $fillable = ['weekday', 'status'];

    public function cat()
    {
        return $this->belongsTo(Cat::class);
    }

}