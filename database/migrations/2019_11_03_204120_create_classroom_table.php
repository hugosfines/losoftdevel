<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClassroomTable extends Migration {

	public function up()
	{
		Schema::create('classroom', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('name');
			$table->bigInteger('cat_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('classroom');
	}
}