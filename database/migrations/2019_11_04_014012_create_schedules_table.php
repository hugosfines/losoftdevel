<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchedulesTable extends Migration {

	public function up()
	{
		Schema::create('schedules', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('day_week')->unsigned();
			$table->time('init_time');
			$table->time('end_time');
			$table->tinyInteger('week')->unsigned();
			$table->bigInteger('group_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('schedules');
	}
}