<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cour_program', function(Blueprint $table) {
            $table->foreign('program_id')->references('id')->on('programs')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('cour_program', function(Blueprint $table) {
            $table->foreign('cour_id')->references('id')->on('cours')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('cour_program', function(Blueprint $table) {
            $table->foreign('level_id')->references('id')->on('levels')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('cat_program', function(Blueprint $table) {
            $table->foreign('cat_id')->references('id')->on('cats')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('cat_program', function(Blueprint $table) {
            $table->foreign('program_id')->references('id')->on('programs')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->foreign('cour_program_id')->references('id')->on('cour_program')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->foreign('teacher_id')->references('id')->on('teachers')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->foreign('classroom_id')->references('id')->on('classroom')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('class', function(Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('attendances', function(Blueprint $table) {
            $table->foreign('class_id')->references('id')->on('class')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('attendances', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('schedules', function(Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('classroom', function(Blueprint $table) {
            $table->foreign('cat_id')->references('id')->on('cats')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
        Schema::table('excuses', function(Blueprint $table) {
            $table->foreign('attendance_id')->references('id')->on('attendances')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
    }

    public function down()
    {
        Schema::table('cour_program', function(Blueprint $table) {
            $table->dropForeign('cour_program_program_id_foreign');
        });
        Schema::table('cour_program', function(Blueprint $table) {
            $table->dropForeign('cour_program_cour_id_foreign');
        });
        Schema::table('cour_program', function(Blueprint $table) {
            $table->dropForeign('cour_program_level_id_foreign');
        });
        Schema::table('cat_program', function(Blueprint $table) {
            $table->dropForeign('cat_program_cat_id_foreign');
        });
        Schema::table('cat_program', function(Blueprint $table) {
            $table->dropForeign('cat_program_program_id_foreign');
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->dropForeign('groups_cour_program_id_foreign');
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->dropForeign('groups_teacher_id_foreign');
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->dropForeign('groups_classroom_id_foreign');
        });
        Schema::table('class', function(Blueprint $table) {
            $table->dropForeign('class_group_id_foreign');
        });
        Schema::table('attendances', function(Blueprint $table) {
            $table->dropForeign('attendances_class_id_foreign');
        });
        Schema::table('attendances', function(Blueprint $table) {
            $table->dropForeign('attendances_user_id_foreign');
        });
        Schema::table('schedules', function(Blueprint $table) {
            $table->dropForeign('schedules_group_id_foreign');
        });
        Schema::table('classroom', function(Blueprint $table) {
            $table->dropForeign('classroom_cat_id_foreign');
        });
        Schema::table('excuses', function(Blueprint $table) {
            $table->dropForeign('excuses_attendance_id_foreign');
        });
    }
}
