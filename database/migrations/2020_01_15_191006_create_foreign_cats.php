<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignCats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('cat_weekdays', function(Blueprint $table) {
            $table->foreign('cat_id')->references('id')->on('cats')
                        ->onDelete('no action')
                        ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_weekdays', function(Blueprint $table) {
            $table->dropForeign('cat_weekdays_cat_id_foreign');
        });
    }
}
