<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarWeeksTable extends Migration {

	public function up()
	{
		Schema::create('calendar_weeks', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->date('init_date');
			$table->date('end_date');
			$table->smallInteger('nweek')->unsigned();
			$table->tinyInteger('week');
		});
	}

	public function down()
	{
		Schema::drop('calendar_weeks');
	}
}