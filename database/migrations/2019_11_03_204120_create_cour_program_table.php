<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourProgramTable extends Migration {

	public function up()
	{
		Schema::create('cour_program', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('program_id')->unsigned();
			$table->bigInteger('cour_id')->unsigned();
			$table->bigInteger('level_id')->unsigned();
			$table->string('name');
			$table->timestamps();
			$table->foreign('program_id')->references('id')->on('programs')
			->onDelete('cascade');
			$table->foreign('cour_id')->references('id')->on('cours')
			->onDelete('cascade');
			$table->foreign('level_id')->references('id')->on('levels')
			->onDelete('cascade');
		});
	}

	public function down()
	{
		Schema::drop('cour_program');
	}
}