<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration {

	public function up()
	{
		Schema::create('groups', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('name');
			$table->bigInteger('cour_program_id')->unsigned();
			$table->bigInteger('teacher_id')->unsigned();
			$table->bigInteger('classroom_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('groups');
	}
}