<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExcusesTable extends Migration {

	public function up()
	{
		Schema::create('excuses', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('excuse_url');
			$table->bigInteger('attendance_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('excuses');
	}
}