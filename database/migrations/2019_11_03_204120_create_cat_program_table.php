<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCatProgramTable extends Migration {

	public function up()
	{
		Schema::create('cat_program', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->bigInteger('cat_id')->unsigned();
			$table->bigInteger('program_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('cat_program');
	}
}