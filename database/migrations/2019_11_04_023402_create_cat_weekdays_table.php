<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCatWeekdaysTable extends Migration {

	public function up()
	{
		Schema::create('cat_weekdays', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->tinyInteger('weekday')->unsigned();
			$table->tinyInteger('status')->unsigned();
			$table->bigInteger('cat_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('cat_weekdays');
	}
}