<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCatsTable extends Migration {

	public function up()
	{
		Schema::create('cats', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
			$table->string('activo');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cats');
	}
}