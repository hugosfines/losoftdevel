<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgramsTable extends Migration {

	public function up()
	{
		Schema::create('programs', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
			$table->timestamps();
			
		});
	}

	public function down()
	{
		Schema::drop('programs');
	}
}