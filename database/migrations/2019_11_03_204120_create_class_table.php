<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClassTable extends Migration {

	public function up()
	{
		Schema::create('class', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('name');
			$table->integer('week');
			$table->integer('tutorial');
			$table->datetime('datetime');
			$table->bigInteger('group_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('class');
	}
}