<?php

use Illuminate\Database\Seeder;
use App\Cour;
use App\CourProgram;
class CourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Cour::create([
        	'name'   => 'SEGURIDAD DE LA INFORMACION',
        	 ]);Cour::create([
            'name'   => 'ELECTIVA: TERAPIAS ALTERNATIVAS Y COMPLEMENTARIAS',
             ]);Cour::create([
            'name'   => 'ELECTIVA: ECOLOGIA Y MEDIO AMBIENTE',
             ]);
        Cour::create([
        	'name'   => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
        	 ]);
        Cour::create([
        	'name'   => 'FUNDAMENTOS PENSAMIENTO HUMANO',
        	 ]);Cour::create([
        	'name'   => 'ECONOMIA Y PRINCIPIOS FINANCIEROS ',
        	 ]);
        Cour::create([
        	'name'   => 'SEMINARIO DE AUTOFORMACION',
        	 ]);
        Cour::create([
        	'name'   => 'ADMINISTRACION DE LA PRODUCCION Y OPERACIONES',
        	 ]);
        Cour::create([
        	'name'   => 'FUNDAMENTOS DE MATEMATICAS',
        	 ]);
        Cour::create([
        	'name'   => 'ALGEBRA LINEAL',
        	 ]);Cour::create([
        	'name'   => 'HERRAMIENTAS INFORMATICAS',
        	 ]);
        Cour::create([
        	'name'   => 'MATEMATICAS FINANCIERAS I',
        	 ]);
        Cour::create([
        	'name'   => 'CONTABILIDAD FINANCIERA',
        	 ]);
        Cour::create([
        	'name'   => 'SEMINARIO PERMANENTE PARA LA AUTOFORMACION',
        	 ]);
        Cour::create([
        	'name'   => 'BIOLOGIA CELULAR Y MICROBIOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'ARQUITECTURA DE SOFTWARE',
        	 ]);
        Cour::create([
        	'name'   => 'INFORMATICA Y TELEMATICA',
        	 ]); Cour::create([
        	'name'   => 'ELEMENTOS DE PROGRAMACION ORIENTADA A OBJETOS',
        	 ]);Cour::create([
        	'name'   => 'INFORMATICA APLICADA SISTEMA DE DISTRIBUCION',
        	 ]);Cour::create([
        	'name'   => 'INFORMATICA APLICADA',
        	 ]);
        Cour::create([
        	'name'   => 'TECNICAS DE COMUNICACION ORAL Y ESCRITA',
        	 ]);
        Cour::create([
        	'name'   => 'QUÍMICA GENERAL E INORGANICA',
        	 ]);
        Cour::create([
        	'name'   => 'MATEMÁTICA APLICADA Y ESTADISTICA',
        	 ]);
        Cour::create([
        	'name'   => 'SOCIO ANTROPOLOGIA DE LA SALUD',
        	 ]);Cour::create([
        	'name'   => 'TECNOLOGIA EN FARMACIA I',
        	 ]);Cour::create([
        	'name'   => 'PROSPECTIVA',
        	 ]);
        Cour::create([
        	'name'   => 'OPTATIVA-MICROFINANZAS',
        	 ]); Cour::create([
        	'name'   => 'OPTATIVA-ADMINISTRACION DEL CAPITAL DEL TRABAJO',
        	 ]);
        Cour::create([
        	'name'   => 'MORFOFISIOLOGIA',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA: FARMACO - EPIDEMIOLOGÍA',
        	 ]); Cour::create([
        	'name'   => 'ELECTIVA: TALLER DE EXPRESION CORPORAL',
        	 ]);  Cour::create([
        	'name'   => 'ELECTIVA: SISTEMAS DE INFORMACION EN SALUD',
        	 ]);Cour::create([
        	'name'   => 'TALLER DE LITERATURA III',
        	 ]);
        Cour::create([
        	'name'   => 'OPTATIVA-ADMINISTRACION DE EMPRESAS DE ECONOMIA SOLIDARIA',
        	 ]);
        Cour::create([
        	'name'   => 'FARMACOLOGIA',
        	 ]);
        Cour::create([
        	'name'   => 'FUNDAMENTOS DE INVESTIGACION',
        	 ]);
        Cour::create([
        	'name'   => 'ANALISIS FINANCIERO',
        	 ]);Cour::create([
        	'name'   => 'ANALISIS DE PROCESOS DE TRABAJO',
        	 ]);
        Cour::create([
        	'name'   => 'GERENCIA ESTRATEGICA',
        	 ]);
        Cour::create([
        	'name'   => 'FARMACIA VETERINARIA',
        	 ]);
        Cour::create([
        	'name'   => 'FARMACIA MAGISTRAL',
        	 ]);
        Cour::create([
        	'name'   => 'AUDITORIA FINANCIERA',
        	 ]);
        Cour::create([
        	'name'   => 'ETICA Y BIOETICA',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICAS EN REGENCIA DE FARMACIA',
        	 ]);
        Cour::create([
        	'name'   => 'COMPETENCIAS COMUNICATIVAS II',
        	 ]);
        Cour::create([
        	'name'   => 'ATENCION INTEGRAL EN SALUD A LA PRIMERA INFANCIA',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA II: DIDACTICA INFANCIA TEMPRANA (1 A 3 AÑOS)',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA III: DIDACTICA EDAD PREESCOLAR (3 A 4 AÑOS)',
        	 ]);
        Cour::create([
        	'name'   => 'SEGURIDAD ALIMENTARIA NUTRICIONAL',
        	 ]);Cour::create([
        	'name'   => 'SEGURIDAD EN TRABAJOS DE ALTO RIESGOS',
        	 ]);
        Cour::create([
        	'name'   => 'SOCIO ANTROPOLOGÍA DE LA EDUCACIÓN',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA - LA INFORMATICA EN LA FORMACION INTEGRAL - PLAN IV -',
        	 ]);
        Cour::create([
        	'name'   => 'PROCESOS DE LECTURA Y ESCRITURA DEL NIÑO',
        	 ]);Cour::create([
        	'name'   => 'FORMACION DEPORTIVA Y ARTE',
        	 ]);
        Cour::create([
        	'name'   => 'INVESTIGACION Y PRACTICA VII: PROSPECTIVA DEL PEDAGOGO INTEGRAL PARA EL PREESCOLAR',
        	 ]);
        Cour::create([
        	'name'   => 'FILOSOFIA Y EDUCACION',
        	 ]);
        Cour::create([
        	'name'   => 'RIESGOS FINANCIEROS',
        	 ]);Cour::create([
        	'name'   => 'ETICA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA III',
        	 ]);Cour::create([
        	'name'   => 'FILOSOFIA DEL ARTE',
        	 ]);Cour::create([
        	'name'   => 'QUIMICA FUNDAMENTAL II',
        	 ]);Cour::create([
        	'name'   => 'INTRODUCCION DE LOS SISTEMAS',
        	 ]);Cour::create([
        	'name'   => 'INTRODUCCION A LAS ARTES APLICADAS',
        	 ]);Cour::create([
        	'name'   => 'INTRODUCCION A LA INGENIERIA EN AGROECOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'TECNOLOGIA Y REGION',
        	 ]);Cour::create([
        	'name'   => 'LOGICA DE SISTEMAS',
        	 ]);Cour::create([
        	'name'   => 'PROCESOS CREATIVOS EN MUSICA',
        	 ]);Cour::create([
        	'name'   => 'PROCESOS CREATIVOS EN TEATRO',
        	 ]);
        Cour::create([
        	'name'   => 'CALCULO UNIVARIADO',
        	 ]);
        Cour::create([
        	'name'   => 'EL INGLES COMO SEGUNDA LENGUA',
        	 ]);
        Cour::create([
        	'name'   => 'INVESTIGACION Y PRACTICA VI: CURRICULO PARA PREESCOLAR',
        	 ]);
        Cour::create([
        	'name'   => 'DISEÑO DEL PROYECTO DE INTERVENCION PEDAGOGICA',
        	 ]);
        Cour::create([
        	'name'   => 'EVALUACION DE LOS PROCESOS EDUCATIVOS',
        	 ]);
        Cour::create([
        	'name'   => 'INVESTIGACION Y PRACTICA VIII: ARTICULACION PREESCOLAR Y PRIMERO DE PRIMARIA',
        	 ]);
        Cour::create([
        	'name'   => 'LINEAMIENTOS DE LA BASICA PRIMARIA',
        	 ]);
        Cour::create([
        	'name'   => 'ANTROPOLOGIA Y EDUCACION',
        	 ]);
        Cour::create([
        	'name'   => 'CONSTITUCION POLITICA',
        	 ]);
        Cour::create([
        	'name'   => 'INVESTIGACION Y PRACTICA IX: NECESIDADES EDUCATIVAS ESPECIALES',
        	 ]);
        Cour::create([
        	'name'   => 'PROYECTO DE INVESTIGACION FORMATIVA II : INTERVENCION PEDAGOGICA',
        	 ]);
        Cour::create([
        	'name'   => 'PROYECTOS EDUCATIVOS ESPECIALES',
        	 ]);
        Cour::create([
        	'name'   => 'COMPETENCIAS COMUNICATIVAS- COGNITIVAS- LECTURA Y ESCRITURA',
        	 ]);
        Cour::create([
        	'name'   => 'INVESTIGACION Y PRACTICA X: LAS TIC EN EDUCACION',
        	 ]);
        Cour::create([
        	'name'   => 'EL ESTUDIO DE LOS ACTOS COMUNICATIVOS',
        	 ]);
        Cour::create([
        	'name'   => 'PROYECTO DE INVESTIGACION FORMATIVA: PEDAGOGIA DE LA LITERATURA',
        	 ]);
        Cour::create([
        	'name'   => 'EVALUACION POR PROCESOS',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA DE VII',
        	 ]); Cour::create([
        	'name'   => 'ELECTIVA DE HUMANIDADES - ARTE Y PINTURA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA DE HUMANIDADES - RECREACION Y DEPORTE',
        	 ]);
        Cour::create([
        	'name'   => 'DESARROLLO DE COMPETENCIAS A TRAVES DEL LENGUAJE',
        	 ]);
        Cour::create([
        	'name'   => 'PEDAGOGIA PSICOSOCIAL DEL LENGUAJE',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA DE VIII',
        	 ]);
        Cour::create([
        	'name'   => 'PEDAGOGIA DE LA LITERATURA INFANTIL',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA DE IX',
        	 ]);
        Cour::create([
        	'name'   => 'LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',
        	 ]);
        Cour::create([
        	'name'   => 'PRODUCCION Y RECEPCION TEXTUAL INTERACTIVA',
        	 ]);
        Cour::create([
        	'name'   => 'PEDAGOGIA Y SOCIEDAD EN COLOMBIA',
        	 ]);
        Cour::create([
        	'name'   => 'FUNDAMENTOS DE ADMINISTRACION',
        	 ]);Cour::create([
        	'name'   => 'RECEPCION Y RESERVAS',
        	 ]);
        Cour::create([
        	'name'   => 'MACROECONOMIA',
        	 ]);
        Cour::create([
        	'name'   => 'ORGANIZACIONES',
        	 ]);
        Cour::create([
        	'name'   => 'FUNDAMENTOS DE CONTABILIDAD',
        	 ]);
        Cour::create([
        	'name'   => 'INFORMATICA Y METODOS CUANTITATIVOS',
        	 ]);
        Cour::create([
        	'name'   => 'COSTOS',
        	 ]);
        Cour::create([
        	'name'   => 'FORMACIÓN DEPORTIVA Y ARTE',
        	 ]);
        Cour::create([
        	'name'   => 'ESTADISTICA BASICA',
        	 ]);
		Cour::create([
        	'name'   => 'MATEMATICAS FINANCIERAS II',
        	 ]);
		Cour::create([
        	'name'   => 'FUNDAMENTOS DE MERCADEO',
        	 ]);
		Cour::create([
        	'name'   => 'INGLES DE NEGOCIOS I',
        	 ]);
		Cour::create([
        	'name'   => 'OPTATIVA - MICROFINANZAS',
        	 ]);
		Cour::create([
        	'name'   => 'PRESUPUESTOS',
        	 ]);
		Cour::create([
        	'name'   => 'ECONOMETRIA BASICA',
        	 ]);Cour::create([
        	'name'   => 'INGLES II',
        	 ]);
		Cour::create([
        	'name'   => 'INGLES I',
        	 ]);Cour::create([
        	'name'   => 'INGLES INTERMEDIO',
        	 ]);Cour::create([
        	'name'   => 'INGLES INTERMEDIO PARA EL TURISMO',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA: EDUCACIÓN EN SALUD',
        	 ]);
        Cour::create([
        	'name'   => 'ECONOMIA Y FINANZAS INTERNACIONALES',
        	 ]);
        Cour::create([
        	'name'   => 'ADMINISTRACION DE FARMACIA',
        	 ]);
        Cour::create([
        	'name'   => 'ESTADISTICA INFERENCIAL',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA: PRIMEROS AUXILIOS',
        	 ]);
        Cour::create([
        	'name'   => 'CONTABILIDAD Y PRESUPUESTOS',
        	 ]);
		Cour::create([
        	'name'   => 'TOXICOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'FARMACOVIGILANCIA Y TECNOVIGILANCIA',
        	 ]);Cour::create([
        	'name'   => 'TEORIA E HISTORIA DE PEDAGOGÍA',
        	 ]);Cour::create([
        	'name'   => 'TEORIA DE SISTEMAS',
        	 ]);Cour::create([
        	'name'   => 'PROGRAMACION DE SISTEMAS INTELIGENTES',
        	 ]);Cour::create([
        	'name'   => 'AUDITORIA Y LEGISLACION INFORMATICA',
        	 ]);Cour::create([
        	'name'   => 'MODELOS DE CONOCIMIENTO',
        	 ]);Cour::create([
        	'name'   => 'MODELOS Y SIMULACION',
        	 ]);Cour::create([
        	'name'   => 'MODELOS DE CONTABLES',
        	 ]);Cour::create([
        	'name'   => 'MODERNIDAD Y POSMODERNIDAD EN EL ARTE',
        	 ]);Cour::create([
        	'name'   => 'MODELOS DE EVALUACION DE PROCESOS',
        	 ]);Cour::create([
        	'name'   => 'INVESTIGACION DE OPERACIONES',
        	 ]);Cour::create([
        	'name'   => 'INVESTIGACION DE MERCADOS ',
        	 ]);Cour::create([
        	'name'   => 'PRODUCCION ORAL Y ESCRITA',
        	 ]);Cour::create([
        	'name'   => 'QUIMICA GENERAL',
        	 ]);Cour::create([
        	'name'   => 'EXPRESION GRAFICA',
        	 ]);Cour::create([
        	'name'   => 'EXPRESION DEL INGLES',
        	 ]);Cour::create([
        	'name'   => 'CALCULO I',
        	 ]);Cour::create([
        	'name'   => 'CALCULO MULTIVARIADO',
        	 ]);Cour::create([
        	'name'   => 'CALCULO II',
        	 ]);Cour::create([
        	'name'   => 'CALCULO III',
        	 ]);Cour::create([
        	'name'   => 'COMPETENCIAS COMUNICATIVAS',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA NUCLEO APROVECHAMIENTO INTEGRAL Y SOSTENIBLE DEL BOSQUE',
        	 ]);Cour::create([
        	'name'   => 'MANEJO Y CONSERVACION DE SUELOS',
        	 ]);Cour::create([
        	'name'   => 'SILVICULTURA DE PLANTACIONES Y BOSQUE NATURAL',
        	 ]);Cour::create([
        	'name'   => 'ESPACIO Y PATRIMONIO CULTURAL Y TURISTICO',
        	 ]);
        	 Cour::create([
        	'name'   => 'FUNDAMENTOS DE PENSAMIENTO HUMANO	',
        	 ]);Cour::create([
        	'name'   => 'ADMINISTRACION DE LA PRODUCCION Y OPERACIONES',
        	 ]);
        	 Cour::create([
        	'name'   => 'FUNDAMENTOS DE SEGURIDAD SOCIAL Y SEGURIDAD Y SALUD EN EL TRABAJO',
        	 ]);Cour::create([
        	'name'   => 'QUÍMICA APLICADA',
        	 ]);Cour::create([
        	'name'   => 'REDES NEURONALES',
        	 ]);Cour::create([
        	'name'   => 'TRABAJO COMUNITARIO',
        	 ]);Cour::create([
        	'name'   => 'MINERIA DE DATOS',
        	 ]);Cour::create([
        	'name'   => 'LA PROPUESTA EDUCATIVA DESDE EL DESARROLLO DEL LENGUAJE',
        	 ]);Cour::create([
        	'name'   => 'LOGISTICA - GESTION Y ORGANIZACION DE EVENTOS',
        	 ]);Cour::create([
        	'name'   => 'ANATOMIA Y FISIOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'RIESGOS LABORALES',
        	 ]);Cour::create([
        	'name'   => 'ETICA PROFESIONAL',
        	 ]);Cour::create([
        	'name'   => 'MEDICINA PREVENTIVA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA I',
        	 ]);Cour::create([
        	'name'   => 'MEDICION DE CONTAMINANTES AMBIENTALES',
        	 ]);Cour::create([
        	'name'   => 'FISICA II',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA II',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA III',
        	 ]);Cour::create([
        	'name'   => 'OPTATIVA II',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA PROFESIONAL II - SISTEMAS DE CONTROL DE INFORMACION',
        	 ]);Cour::create([
            'name'   => 'ELECTIVA PROFESIONAL I',
             ]);Cour::create([
        	'name'   => 'ELECTIVA PROFESIONAL III - INFORMATICA FORENSE',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA HUMANIDADES - RECUPERACION Y DEPORTE',
        	 ]);Cour::create([
        	'name'   => 'FORMULACION Y EVALUACION DE PROYECTOS',
        	 ]);Cour::create([
        	'name'   => 'SEMINARIO I: CARACTERIZACION DEL SISTEMA DE RIESGOS LABORALES',
        	 ]);Cour::create([
        	'name'   => 'SEMINARIO II: ANALISIS DE CONDICIONES DE SALUD Y DE TRABAJO',
        	 ]);Cour::create([
        	'name'   => 'FISICA APLICADA',
        	 ]);Cour::create([
        	'name'   => 'MEDICINA DEL TRABAJO',
        	 ]);Cour::create([
        	'name'   => 'PELIGROS PSICOSOCIALES',
        	 ]);Cour::create([
        	'name'   => 'PELIGROS EN LAS CONDICIONES DE HIGIENE',
        	 ]);Cour::create([
        	'name'   => 'VIGILANCIA EPIDEMIOLOGICA',
        	 ]);Cour::create([
        	'name'   => 'ESTADISTICA',
        	 ]);Cour::create([
        	'name'   => 'MANEJO AGROECOLOGICO DEL SUELO',
        	 ]);Cour::create([
        	'name'   => 'ECUACIONES DIFERENCIALES',
        	 ]);Cour::create([
        	'name'   => 'ECOLOGIA DEL INSECTO',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA II',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA II - DISCIPLINAR',
        	 ]);Cour::create([
        	'name'   => 'PELIGROS EN LAS CONDICIONES DE SEGURIDAD',
        	 ]);Cour::create([
        	'name'   => 'TOXICOLOGIA OCUPACIONAL',
        	 ]);Cour::create([
        	'name'   => 'ERGONOMIA Y PELIGROS BIOMECANICOS',
        	 ]);Cour::create([
        	'name'   => 'CONTINGENCIAS EN EL TRABAJO',
        	 ]);Cour::create([
        	'name'   => 'INGENIERIA DE SOFTWARE',
        	 ]);Cour::create([
        	'name'   => 'INGENIERIA DE NEGOCIOS',
        	 ]);Cour::create([
        	'name'   => 'COSTOS Y PRESUPUESTOS',
        	 ]);Cour::create([
        	'name'   => 'GESTION Y DESARROLLO DE DESTINOS TURISTICOS',
        	 ]);Cour::create([
        	'name'   => 'SISTEMAS INTEGRADOS DE GESTION',
        	 ]);Cour::create([
        	'name'   => 'SISTEMAS OPERATIVOS',
        	 ]);Cour::create([
        	'name'   => 'SISTEMAS INTEGRADOS DE CALIDAD',
        	 ]);Cour::create([
        	'name'   => 'DESARROLLO INTEGRAL INFANTIL I',
        	 ]);Cour::create([
        	'name'   => 'FILOSOFIA DE LA EDUCACIÓN',
        	 ]);Cour::create([
        	'name'   => 'PRACTICA IV: DIDACTICA TRANSICIÓN (5 A 6 AÑOS)',
        	 ]);Cour::create([
        	'name'   => 'PRACTICA EMPRESARIAL',
        	 ]);Cour::create([
        	'name'   => 'INVESTIGACION I',
        	 ]); Cour::create([
        	'name'   => 'INVESTIGACION II',
        	 ]); Cour::create([
        	'name'   => 'TECNOLOGIA EN FARMACIA II',
        	 ]); Cour::create([
        	'name'   => 'OPTATIVA - PSICOLOGIA EMPRESARIAL',
        	 ]);Cour::create([
        	'name'   => 'OPTATIVA - ADMINISTRACION DE EMPRESAS DE ECONOMIA SOLIDARIA',
        	 ]);Cour::create([
        	'name'   => 'OPTATIVA I: TECNICAS PARA LA INVESTIGACION  DE RIESGOS LABORALES',
        	 ]);Cour::create([
        	'name'   => 'OPTATIVA II: HERRAMIENTAS INFORMATICAS PARA LA INVESTIGACION Y EDUCACIÓN EN SEGURIDAD Y SALUD EN EL TRABAJO ',
        	 ]);Cour::create([
        	'name'   => 'ADMINISTRACION DE TALENTO HUMANO',
        	 ]);Cour::create([
        	'name'   => 'OPTATIVA - LITERATURA: LITERATURA INFANTIL Y MUSICA ',
        	 ]);Cour::create([
        	'name'   => 'NIVELES DE LA LENGUA I: FONETICA Y FONOLOGÍA',
        	 ]);Cour::create([
        	'name'   => 'AMBIENTE- DESARROLLO Y SOCIEDAD  ',
        	 ]);
        	Cour::create([
        	'name'   => 'CONSTITUCIÓN POLÍTICA',
        	 ]);Cour::create([
        	'name'   => 'ECOTURISMO',
        	 ]);Cour::create([
        	'name'   => 'INGLES BASICO PARA EL TURISMO',
        	 ]);Cour::create([
        	'name'   => 'INGLES AVANZADO PARA EL TURISMO',
        	 ]);
        	Cour::create([
        	'name'   => 'SEMINARIO PERMANENTE PARA LA AUTOFOMACIÓN',
        	 ]);Cour::create([
        	'name'   => 'SEMINARIO PROP DE MEJOR DE LAS CONDICIONES DE SALUD Y DE TRABAJO',
        	 ]);Cour::create([
        	'name'   => 'PRESENTACION PROYECTO DE INVESTIGACION DE V A VII NIVEL  ',
        	 ]);Cour::create([
        	'name'   => 'PRESENTACION PROYECTO DE INVESTIGACION DE IV NIVEL  ',
        	 ]);
		Cour::create([
        	'name'   => 'GERENCIA DE MERCADEO',
        	 ]);Cour::create([
        	'name'   => 'DIMENSION ESTETICA',
        	 ]);Cour::create([
        	'name'   => 'GERENCIA DEL TALENTO HUMANO',
        	 ]);
		Cour::create([
        	'name'   => 'MATEMÁTICA DE LO COTIDIANO',
        	 ]);
		Cour::create([
        	'name'   => 'QUÍMICA FUNDAMENTAL I',
        	 ]);Cour::create([
        	'name'   => 'DIAGNOSTICO EMPRESARIAL',
        	 ]);Cour::create([
        	'name'   => 'DIAGNOSTICO DE CONDICIONES DE SALUD Y TRABAJO',
        	 ]);
		Cour::create([
        	'name'   => 'SEMINARIO PERMANENTE DE AUTOFORMACION',
        	 ]);Cour::create([
        	'name'   => 'PRESENTACION PROYECTO DE INVESTIGACION DE IV NIVEL',
        	 ]);
		Cour::create([
        	'name'   => 'PRACTICA I BÁSICA PRIMARIA',
        	 ]);Cour::create([
        	'name'   => 'FORMAS MUSICALES AUTOCTONAS',
        	 ]);
		Cour::create([
        	'name'   => 'PRACTICA II BÁSICA SECUNDARIA',
        	 ]);
		Cour::create([
        	'name'   => 'QUÍMICA FUNDAMENTAL II',
        	 ]);
		Cour::create([
        	'name'   => 'CALCULO',
        	 ]);
		Cour::create([
        	'name'   => 'QUIMICA FUNDAMENTAL I',
        	 ]);Cour::create([
        	'name'   => 'QUIMICA ORGANICA',
        	 ]);Cour::create([
        	'name'   => 'QUIMICA DEL SUELO',
        	 ]);
		Cour::create([
        	'name'   => 'MICROBIOLOGÍA GENERAL',
        	 ]);
		Cour::create([
        	'name'   => 'COMUNICACIÓN CIENTIFICA',
        	 ]);Cour::create([
        	'name'   => 'MUNDO ANIMAL: VERTEBRADOS',
        	 ]);
		Cour::create([
        	'name'   => 'PENSAMIENTO CULTURA Y EDUCACIÓN',
        	 ]);
		Cour::create([
        	'name'   => 'PRACTICA III EDUCACIÓN MEDIA',
        	 ]);Cour::create([
        	'name'   => 'TURISMO DE LA NATURALEZA',
        	 ]);
		Cour::create([
        	'name'   => 'TECNOLOGíA DE LA INFORMACIÓN Y LA COMUNICACIÓN',
        	 ]);Cour::create([
        	'name'   => 'MEDINA PREVENTIVA',
        	 ]);
		Cour::create([
        	'name'   => 'BOTÁNICA TAXONÓMICA Y SISTEMÁTICA',
        	 ]);Cour::create([
        	'name'   => 'INFORMATICA BASICA',
        	 ]);Cour::create([
        	'name'   => 'TAXONOMIA DEL SUELO',
        	 ]);
		Cour::create([
        	'name'   => 'PRACTICA IV GRADO 1°2°3°',
        	 ]);
		Cour::create([
        	'name'   => 'INVESTIGACIÓN I',
        	 ]);
		Cour::create([
        	'name'   => 'BIOQUÍMICA',
        	 ]);
		Cour::create([
        	'name'   => 'ÉTICA Y CULTURA DE LA PAZ',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA TECNOLOGICA - ADMINISTRACION DE SERVIDORES',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA TECNOLOGICA - SEGURIDAD DE REDES',
        	 ]);
		Cour::create([
        	'name'   => 'ELECTIVA SOCIOHUMANISTICA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVAS',
        	 ]);Cour::create([
        	'name'   => 'LA PEDAGOGIA DEL INGLES EN EL PREESCOLAR',
        	 ]);
		Cour::create([
        	'name'   => 'DIDACTICA DE LAS CIENCIAS NATURALES I',
        	 ]);
		Cour::create([
        	'name'   => 'FISICA FUNDAMENTAL',
        	 ]);
		Cour::create([
        	'name'   => 'ELECTIVA DISCIPLINAR',
        	 ]);
		Cour::create([
        	'name'   => 'BIOFISICA',
        	 ]);
		Cour::create([
        	'name'   => 'DIDACTICA DE LAS CIENCIAS NATURALES II',
        	 ]);
		Cour::create([
        	'name'   => 'METODOLOGIA DE LA INVESTIGACION',
        	 ]);Cour::create([
        	'name'   => 'METODOLOGIA DE DISEÑO DE SOFTWARE',
        	 ]);Cour::create([
        	'name'   => 'DISEÑO DE PRODUCTO TURISTICO',
        	 ]);Cour::create([
        	'name'   => 'DISEÑO DE PROYECTO DE INTERVENCION PEDAGOGICA ',
        	 ]);Cour::create([
        	'name'   => 'DISEÑO DE REDES',
        	 ]);Cour::create([
        	'name'   => 'DISEÑO EXPERIMENTAL',
        	 ]);
		Cour::create([
        	'name'   => 'ETICA Y VALORES',
        	 ]);
		Cour::create([
        	'name'   => 'PRACTICA PEDAGOGICA I',
        	 ]);Cour::create([
        	'name'   => 'PRACTICA PEDAGOGICA II',
        	 ]);
		Cour::create([
        	'name'   => 'PROYECTOS PEDAGOGICOS',
        	 ]);
		Cour::create([
        	'name'   => 'FISIOLOGIA VEGETAL',
        	 ]);
		Cour::create([
        	'name'   => 'GEOGRAFIA Y MEDIO AMBIENTE',
        	 ]);
		Cour::create([
        	'name'   => 'FISIOLOGIA ANIMAL',
        	 ]);Cour::create([
        	'name'   => 'ANIMACION SOCIOCULTURAL',
        	 ]);
		Cour::create([
        	'name'   => 'ECOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'AMBIENTE- DESARROLLO Y SOCIEDAD',
        	 ]);
		Cour::create([
        	'name'   => 'IMPACTO Y GESTION AMBIENTAL',
        	 ]);
		Cour::create([
        	'name'   => 'GENETICA Y EVOLUCION',
        	 ]);Cour::create([
        	'name'   => 'GENETICA',
        	 ]);Cour::create([
        	'name'   => 'SUSTENTACION PROYECTO',
        	 ]);
        Cour::create([
        	'name'   => 'LECTURA Y ESCRITURA EN LA UNIVERSIDAD',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA DE OBSERVACIÓN I: CONTEXTUALIZACION',
        	 ]);Cour::create([
        	'name'   => 'PRACTICA DE OBSERVACIÓN III: ENSEÑANZA DE LA LITER- Y LA LENGUA',
        	 ]);Cour::create([
        	'name'   => 'PRACTICA DE OBSERVACIÓN IV: ENSEÑANZA DE LA LITER- Y LA LENGUA CASTELLANA: SECUNDARIA Y MEDIA',
        	 ]);
        Cour::create([
        	'name'   => 'LENGUAJE- COGNITIVO Y SOCIEDAD',
        	 ]);Cour::create([
        	'name'   => 'RITMO Y MOVIMIENTO EN LA DANZA',
        	 ]);
        Cour::create([
        	'name'   => 'TECNOLOGIA DE LA INFORMACIÓN Y LA COMUNICACIÓN',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA DE OBSERVACIÓN II: PRACTICAS PEDAGOGICAS',
        	 ]);
        Cour::create([
        	'name'   => 'PEDAGOGÍA Y SOCIEDAD EN COLOMBIA',
        	 ]);
        Cour::create([
        	'name'   => 'ESTUDIO DEL TEXTO LITERARIO I',
        	 ]);
        Cour::create([
        	'name'   => 'ESTUDIO DEL TEXTO LITERARIO II',
        	 ]);Cour::create([
        	'name'   => 'ESTUDIO DEL TEXTO LITERARIO III',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA DE OBSERVACIÓN III: ENSEÑANZA DE LA LITER- Y LA LENGUA',
        	 ]);Cour::create([
        	'name'   => 'MATEMATICAS APLICADA Y ESTADISTICA	',
        	 ]);Cour::create([
        	'name'   => 'ESTADO REGIONAL DE ARTE',
        	 ]);Cour::create([
        	'name'   => 'APLICACION DE LA PROGRAMACION ORIENTADA A OBJETOS',
        	 ]);
        Cour::create([
        	'name'   => 'LECTURA Y ESCRITURA EN LA ESCUELA',
        	 ]);
        Cour::create([
        	'name'   => 'NIVELES DE LA LENGUA II: MORFO-SINTAXIS',
        	 ]);
        Cour::create([
        	'name'   => 'EPISTEMOLOGIA Y CONSTRUCCIÓN DEL PENSAMIENTO',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA DE OBSERVACIÓN IV: ENSEÑANZA DE LA LITER- Y LA LENGUA CASTELLANA : SECUNDARIA Y MEDIA',
        	 ]);
        Cour::create([
        	'name'   => 'NIVELES DE LA LENGUA III: SEMÁNTICA Y PRAGMÁTICA',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA DE FORMACIÓN I: DIDÁCTICA DEL LENGUAJE',
        	 ]);
        Cour::create([
        	'name'   => 'EPISTEMOLOGIA Y CONSTRUCCIÓN DEL PENSAMIENTO',
        	 ]);
        Cour::create([
        	'name'   => 'INVESTIGACIÓN II',
        	 ]);
        Cour::create([
        	'name'   => 'ETICA Y CULTURA DE LA PAZ',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA DE FORMACIÓN II: DIDACTICA DE LA LITERATURA',
        	 ]);
        Cour::create([
        	'name'   => 'ADMINISTRACIÓN Y LEGISLACIÓN EDUCATIVA',
        	 ]);
        Cour::create([
        	'name'   => 'SEMINARIO DE PRACTICA PROFESIONAL I: BÁSICA PRIMARIA',
        	 ]);Cour::create([
        	'name'   => 'PROFUNDIZACION EN PROGRAMACION ORIENTADA A OBJETOS',
        	 ]);
        Cour::create([
        	'name'   => 'SEMINARIO DE LITERATURA COLOMBIANA',
        	 ]);
        Cour::create([
        	'name'   => 'SEMINARIO DE PRACTICA PROFESIONAL II: BÁSICA SECUNDARIA',
        	 ]);
        Cour::create([
        	'name'   => 'COMPETENCIAS CUMUNICATIVAS I',
        	 ]);
        Cour::create([
        	'name'   => 'HISTORIA Y EPISTEMOLOGIA DE LA EDUCACIÓN',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA I: DIDACTICA PRIMERA EDAD (0 A 12 MESES)',
        	 ]);
        Cour::create([
        	'name'   => 'DESARROLLO INTEGRAL INFANTIL II',
        	 ]);
        Cour::create([
        	'name'   => 'GERENCIA FINANCIERA',
        	 ]);
        Cour::create([
        	'name'   => 'EDUCACION PARA LA SEXUALIDAD',
        	 ]);Cour::create([
        	'name'   => 'GESTION DE INFORMACION I',
        	 ]);Cour::create([
        	'name'   => 'GESTION DE INFORMACION II',
        	 ]);
        Cour::create([
        	'name'   => 'GESTION DEL TALENTO HUMANO',
        	 ]);Cour::create([
        	'name'   => 'GESTION DE SERVICIOS DE LA HOSPITALIDAD',
        	 ]);Cour::create([
        	'name'   => 'GESTION DE SERVICIOS DE ALIMENTACION',
        	 ]);Cour::create([
        	'name'   => 'GESTION DE SERVICIOS DE VIAJES Y TURISMO',
        	 ]);
        Cour::create([
        	'name'   => 'PROCESOS DE APRENDIZAJE EN EL NIÑO',
        	 ]);
        Cour::create([
        	'name'   => 'OPTATIVA LITERATURA: LITERATURA INFANTIL Y MÚSICA',
        	 ]);
        Cour::create([
        	'name'   => 'FAMILIA E INFANCIA',
        	 ]);
        Cour::create([
        	'name'   => 'PROCESOS DEL LENGUAJE DEL NIÑO',
        	 ]);
        Cour::create([
        	'name'   => 'ESTIMULACIÓN PARA EL DESARROLLO INFANTIL',
        	 ]);Cour::create([
        	'name'   => 'EL DISCURSO LITERARIO PSICOCRITICA',
        	 ]);
        Cour::create([
        	'name'   => 'FISICA GENERAL',
        	 ]); Cour::create([
        	'name'   => 'FISICA',
        	 ]);Cour::create([
        	'name'   => 'COCINA BASICA DE MESA Y BAR',
        	 ]);
        Cour::create([
        	'name'   => 'ETICA CIUDADANIA Y EDUCACIÓN PARA LA PAZ',
        	 ]);
        Cour::create([
        	'name'   => 'PRACTICA V: APS (0 A 2 AÑOS)',
        	 ]);
        Cour::create([
        	'name'   => 'PEDAGOGÍAS CONTEMPORANES',
        	 ]);
        Cour::create([
        	'name'   => 'ELECTIVA IX',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA I - SOCIOHUMANISTICA',
        	 ]);Cour::create([
        	'name'   => 'INGLES BASICO ',
        	 ]);Cour::create([
        	'name'   => 'INGLES BASICO CON ENFASIS EN PRONUNCIACION',
        	 ]);Cour::create([
        	'name'   => 'INGLES BASICO CON ENFASIS LECTURA Y ESCRITURA',
        	 ]);
        Cour::create([
        	'name'   => 'INGLES DE NEGOCIOS II',
        	 ]);Cour::create([
        	'name'   => 'SISTEMAS AGROFORESTALES',
        	 ]);
        Cour::create([
        	'name'   => 'INGLES TECNICO',
        	 ]); Cour::create([
        	'name'   => 'PARQUES Y RESERVAS NATURALES',
        	 ]);Cour::create([
        	'name'   => 'APROVECHAMIENTO FORESTAL',
        	 ]);Cour::create([
        	'name'   => 'FUNDAMENTOS DE FOTOGRAMETRIA Y FOTOINTERPRETACION',
        	 ]);Cour::create([
        	'name'   => 'FARMACOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'MATEMATICAS DE LO COTIDIANO',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - TALLER INTEGRAL DE TEATRO - PLAN IV',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - TALLER INTEGRAL DE DANZA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - EDUCACION SEXUAL - PLAN IV -',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - PEDAGOGIA - CREATIVIDAD Y LUDICA - PLAN IV -',
        	 ]);Cour::create([
        	'name'   => 'TALLER DE LITERATURA I',
        	 ]);Cour::create([
        	'name'   => 'TALLER DE LITERATURA II',
        	 ]);Cour::create([
        	'name'   => 'TALLER DE EXPRESION Y CREACION ARTISTICA',
        	 ]);Cour::create([
        	'name'   => 'TALLER INTEGAL DE TEATRO',
        	 ]);Cour::create([
        	'name'   => 'TALLER INTEGAL DE MUSICA',
        	 ]);Cour::create([
        	'name'   => 'PROBABILIDAD I',
        	 ]);Cour::create([
        	'name'   => 'PROBABILIDAD II',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - PROYECTOS EDUCATIVOS INSTITUCIONALES - PLAN IV -',
        	 ]);
       		Cour::create([
        	'name'   => 'ELECTIVA - PROBLEMAS DE COMPORTAMIENTO Y DEL APRENDIZAJE INFANTIL - PLAN IV -', ]);
        Cour::create([
        	'name'   => 'ELECTIVA - NUCLEO PROTECCION Y RECUPERACION DE ECOSISTEMAS FORESTALES IV NIVEL', ]);
        Cour::create([
        	'name'   => 'ELECTIVA - NUCLEO PROTEECION Y RECUPERACION DE ECOSISTEMAS FORESTALES RECURSOS PROMISORIOS ', ]);
        	Cour::create([
        	'name'   => 'ELECTIVA - NUCLEO DESARROLO HUMANO Y HABILIDADES COMUNICATIVAS',
        	 ]);Cour::create([
        	'name'   => 'CLIMATOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - NUCLEO RAZONAMIENTO MATEMATICO',
        	 ]);
        Cour::create([
        	'name'   => 'LEGISLACION EMPRESARIAL',
        	 ]); Cour::create([
        	'name'   => 'PARQUES Y RESERVAS NATURALES',
        	 ]);Cour::create([
        	'name'   => 'LEGISLACION EN SEGURIDAD Y SALUD EN EL TRABAJO',
        	 ]);
        Cour::create([
        	'name'   => 'OPTATIVA - ADMINISTRACION DEL CAPITAL TRABAJO',
        	 ]);Cour::create([
        	'name'   => 'EDUCACION AMBIENTAL',
        	 ]);Cour::create([
            'name'   => 'ADMINISTRACION DEL TALENTO HUMANO',
             ]);
        Cour::create([
        	'name'   => 'MERCADO DE CAPITALES',
        	 ]);
        Cour::create([
        	'name'   => 'PROSPECTIVA',
        	 ]);Cour::create([
        	'name'   => 'LEGISLACION TRIBUTARIA',
        	 ]);Cour::create([
        	'name'   => 'EVALUACION Y GERENCIA DE PROYECTOS',
        	 ]);Cour::create([
        	'name'   => 'GERENCIA DE PROYECTOS',
        	 ]);Cour::create([
        	'name'   => 'JUEGOS GERENCIALES',
        	 ]);Cour::create([
        	'name'   => 'POLITICA DE NEGOCIOS',
        	 ]);Cour::create([
        	'name'   => 'POLITICA Y PLANEACION DEL TURISMO',
        	 ]);
        Cour::create([
        	'name'   => 'INVENTARIO FORESTAL',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - NUCLEO PROTECCION Y RECUPERACION DE ECOSISTEMAS FORESTALES RECURSOS PROMISORIOS',
        	 ]);Cour::create([
        	'name'   => 'DASOMETRIA',
        	 ]);Cour::create([
        	'name'   => 'CONTABILIDAD Y ADMINISTRACION DE EMPRESAS FORESTALES',
        	 ]);Cour::create([
        	'name'   => 'EDAFOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'PROPIEDAD Y TECNOLOGIA DE LA MADERA',
        	 ]);Cour::create([
        	'name'   => 'BIOLOGIA FUNDAMENTAL',
        	 ]);Cour::create([
        	'name'   => 'BIOLOGIA GENERAL',
        	 ]);Cour::create([
        	'name'   => 'SUBJETIVIDAD Y EDUCACIÓN',
        	 ]);Cour::create([
        	'name'   => 'BOTANICA GENERAL Y FISIOLOGIA',
        	 ]);Cour::create([
        	'name'   => 'BOTANICA',
        	 ]);Cour::create([
        	'name'   => 'QUÍMICA GENERAL E INORGANICA',
        	 ]);Cour::create([
        	'name'   => 'TEORÍA E HISTORIA DE LA DIDÁCTICA',
        	 ]);Cour::create([
        	'name'   => 'ESTADISTICA FUNDAMENTAL',
        	 ]);Cour::create([
        	'name'   => 'CONSTITUCION POLTICA Y COMPETENCIAS CIUDADANAS',
        	 ]);Cour::create([
        	'name'   => 'BIOLOGIA HUMANA',
        	 ]);Cour::create([
        	'name'   => 'EVALUACION DE PROCESOS',
        	 ]);Cour::create([
        	'name'   => 'LOS ESTUDIOS DEL DISCURSO ',
        	 ]);Cour::create([
        	'name'   => 'LINEAMIENTOS CURRICULARES Y EVALUACION DE LOS PROCESOS EDUCATIVOS',
        	 ]);Cour::create([
        	'name'   => 'LEGISLACION Y ADMINISTRACION EDUCATIVA',
        	 ]);Cour::create([
        	'name'   => 'ELECTIVA - JUEGOS Y DEPORTES - PLAN IV -',
        	 ]);Cour::create([
        	'name'   => 'ADMINISTRACION Y LEGISLACION EDUCATIVA',
        	 ]);Cour::create([
        	'name'   => 'ADMINISTRACION FINANCIERA',
        	 ]);Cour::create([
        	'name'   => 'METODOS NUMERICOS',
        	 ]);Cour::create([
        	'name'   => 'MATEMATICAS DISCRETAS II',
        	 ]);Cour::create([
        	'name'   => 'TEMATICAS DE COMUNICACION ORAL Y ESCRITA',
        	 ]);Cour::create([
        	'name'   => 'COMUNICACION DEL INGLES',
        	 ]);Cour::create([
            'name'   => 'RESPONSABILIDAD SOCIAL EMPRESARIAL',
             ]);Cour::create([
            'name'   => 'ELECTIVA HUMANIDADES - RECREACION Y DEPORTE',
             ]);
    }
}
