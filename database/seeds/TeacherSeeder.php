<?php

use Illuminate\Database\Seeder;
use App\Teacher;
use App\Classroom;
class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		Classroom::create([
			'name'   => 'AULA 01' , 'cat_id' => '1' ]);
		Classroom::create([
			'name'   => 'AULA 02' , 'cat_id' => '1']);
		Classroom::create([
			'name'   => 'SALON 01' , 'cat_id' => '2']);
		Classroom::create([
			'name'   => 'SALON 02' , 'cat_id' => '3']);
		Classroom::create([
			'name'   => 'SALON 03' , 'cat_id' => '4']);
		Classroom::create([
			'name'   => 'SALON 04' , 'cat_id' => '5']);
		Classroom::create([
			'name'   => 'SALON 05' , 'cat_id' => '6']);
		Classroom::create([
			'name'   => 'SALON 301' , 'cat_id' => '7']);
		Classroom::create([
			'name'   => 'SALON 07' , 'cat_id' => '8']);
		Classroom::create([
			'name'   => 'SALON 106' , 'cat_id' => '9']);
		Classroom::create([
			'name'   => 'SALON 105' , 'cat_id' => '10']);
		Classroom::create([
			'name'   => 'SALON 107' , 'cat_id' => '11']);
		Classroom::create([
			'name'   => 'SALON 108' , 'cat_id' => '12']);
		Classroom::create([
			'name'   => 'SALON 109' , 'cat_id' => '13']);
		Classroom::create([
			'name'   => 'SALON 111' , 'cat_id' => '14']);
		Classroom::create([
			'name'   => 'AULA 119' , 'cat_id' => '15']);
		Classroom::create([
			'name'   => 'SALON 120' , 'cat_id' => '16']);
		Classroom::create([
			'name'   => 'SALON 121' , 'cat_id' => '17']);
		Classroom::create([
			'name'   => 'SALON 122' , 'cat_id' => '18']);
		Classroom::create([
			'name'   => 'SALON 123' , 'cat_id' => '19']);
		Classroom::create([
			'name'   => 'SALON 124' , 'cat_id' => '20']);
		Classroom::create([
			'name'   => 'AULA 200' , 'cat_id' => '21']);
		Teacher::create([
        	'name'   => 'FABRERAS RODADO CARLOS JESUS'       	 ]);
		Teacher::create([
        	'name'   => 'RODRIGUEZ ROJAS ADRIANA MARIA'    	 ]);
		Teacher::create([
        	'name'   => 'RODRIGUEZ RIAÑO GERMAN'    	 ]);
		Teacher::create([
        	'name'   => 'GALINDEZ GARCIA ORLANDO'    	 ]);
		Teacher::create([
        	'name'   => 'GARCIA ZAMBRANO LILIANA ZAMBRANO'    	 ]);
		Teacher::create([
        	'name'   => 'CRISTANCHO MENDOZA FRANCY VIVIANA'    	 ]);
		Teacher::create([
        	'name'   => 'MAYORGA BECERRA ZORAIDA'    	 ]);
		Teacher::create([
        	'name'   => 'MAYA SANCHEZ RAUL'    	 ]);
		Teacher::create([
        	'name'   => 'HOYOS ARANGO ENRIQUE '    	 ]);
		Teacher::create([
        	'name'   => 'REDONDO PALMA JANIO LUIS'       	 ]);
		Teacher::create([
        	'name'   => 'PAEZ ANGARITA ANGELICA'       	 ]);
		Teacher::create([
        	'name'   => 'CASTILLO MORENO FABIOLA CENITH'    	 ]);
		Teacher::create([
        	'name'   => 'RENGIFO SUAREZ CLAUDIA'    	 ]);
		Teacher::create([
        	'name'   => 'AREVALO GOMEZ MARIA DEL PILAR'    	 ]);
		Teacher::create([
        	'name'   => 'SUAREZ MOREA CHRISTIAN FELIPE'    	 ]);
		Teacher::create([
        	'name'   => 'CURREA DE LA CRUZ LENIN'    	 ]);
		Teacher::create([
        	'name'   => 'SALCEDO SILVERA ROSA MARIA'    	 ]);
		Teacher::create([
        	'name'   => 'BREIVA MACIAS CINDY LORENA'    	 ]);
		Teacher::create([
        	'name'   => 'LOPEZ SAMPER ANTONIO MARIO'    	 ]);
		Teacher::create([
        	'name'   => 'MANGONES ORTEGA JHON RAFAEL '    	 ]);
		Teacher::create([
        	'name'   => 'DIAZ AVENDAÑO JHON HADMINTON'    	 ]);
        Teacher::create([
            'name'   => 'DIAZ RODRIGUEZ GENNY PAOLA'       ]);
		Teacher::create([
        	'name'   => 'BARANDICA SABALZA EUNICE ESTHER'    	 ]);
		Teacher::create([
        	'name'   => 'BARAHONA RAMIREZ JOSE EFRAIN'    	 ]);
		Teacher::create([
        	'name'   => 'GARCIA GARCIA GLORIA BELINDA'    	 ]);
		Teacher::create([
        	'name'   => 'GARCIA DEVIA BOLEMA SMIDIA'    	 ]);
		Teacher::create([
        	'name'   => 'LOSADA COLLAZOS REYNALDO'    	 ]);
		Teacher::create([
        	'name'   => 'BARRERO SAENS MARIA CRISTINA'    	 ]);
		Teacher::create([
        	'name'   => 'QUIROZ CARO EDISSON'    	 ]);
		Teacher::create([
        	'name'   => 'POLONIA REYES CLAUDIA LORENA'    	 ]);
		Teacher::create([
        	'name'   => 'PORTELA LOZANO LIBARDO'    	 ]);
		Teacher::create([
        	'name'   => 'CORREDOR TORRES JOSE EDUARDO'    	 ]);
		Teacher::create([
        	'name'   => 'CORREA QUINCENO JUAN CARLOS'    	 ]);
		Teacher::create([
        	'name'   => 'CASTELLANOS ARCINIEGAS EDGAR'    	 ]);
		Teacher::create([
        	'name'   => 'CARO PINEDA LUIS GUILLERMO'    	 ]);
		Teacher::create([
        	'name'   => 'DE ARCO BALLESTEROS JOSE LUIS'    	 ]);
		Teacher::create([
        	'name'   => 'BALLEN CASTIBLANCO BEIMAN ANDREY'    	 ]);
		Teacher::create([
        	'name'   => 'GUERRERO MENDOZA EDGARDO'    	 ]);
        Teacher::create([
            'name'   => 'BLANCO POLANIA IVAN ANDRES'        ]);
		Teacher::create([
        	'name'   => 'GAMBOA RUIZ CARLOS ANDRES'    	 ]);
		Teacher::create([
        	'name'   => 'GUEVARA NAYZIR JANN'    	 ]);
		Teacher::create([
        	'name'   => 'ALFORD MUÑOZ OSCAR JAVIER'    	 ]);
		Teacher::create([
        	'name'   => 'ROCHA ROCHA PATRICIA MARIA'    	 ]);
		Teacher::create([
        	'name'   => 'ROCHA SALEME HECTOR'    	 ]);
		Teacher::create([
        	'name'   => 'AVILA DE LA HOZ RICARDO ENRIQUE'    	 ]);
		Teacher::create([
        	'name'   => 'SOTO CARDONA JUAN CARLOS'    	 ]);
		Teacher::create([
        	'name'   => 'CARDONA SOTO SANDRA LUCY'    	 ]);
		Teacher::create([
        	'name'   => 'SANCHEZ ALVAREZ LUZ STELLA'    	 ]);
		Teacher::create([
        	'name'   => 'ROJAS PARRA JORGE EDY'    	 ]);
		Teacher::create([
        	'name'   => 'ROJAS TORRES HERMES'    	 ]);
		Teacher::create([
        	'name'   => 'AVILA DE LA HOZ RICARDO ENRIQUE'    	 ]);
		Teacher::create([
        	'name'   => 'VALDIVIESO MAÑOSCA OMAR ALFONSO '    	 ]);
		Teacher::create([
        	'name'   => 'VALENCIA PEREZ MARIA DEL PILAR '    	 ]);
		Teacher::create([
        	'name'   => 'PEREZ GUZMAN MILENA ROCIO'    	 ]);
		Teacher::create([
        	'name'   => 'RAMIREZ GOMEZ LINDA EVELIN '    	 ]);
        Teacher::create([
            'name'   => 'ORJUELA VARGAS SERGIO ALEJANDRO '        ]);
        Teacher::create([
            'name'   => 'VARGAS GUARNIZO MARIA TEREZA'        ]);
        Teacher::create([
            'name'   => 'APONTE LOPEZ SAUL OSBALDO'        ]);
		Teacher::create([
        	'name'   => 'GOMEZ CAMACHO JORGE ELIECER'    	 ]);
		Teacher::create([
        	'name'   => 'NARANJO ACOSTA WILLIAN GUILLERMO'    	 ]);
		Teacher::create([
        	'name'   => 'ACOSTA ALVAREZ ORLANDO'       	 ]);
		Teacher::create([
        	'name'   => 'DEAZA CASTILLO ANDREA JOHANNE'       	 ]);
		Teacher::create([
        	'name'   => 'APARICIO GONZALEZ ILIANA MARITZA'       	 ]);
		Teacher::create([
        	'name'   => 'OSORIO MOSQUERA SAYDEDD'    	 ]);
		Teacher::create([
        	'name'   => 'COMAS SALAZAR CARLOS ALBERTO'    	 ]);
		Teacher::create([
        	'name'   => 'HUERTAS ARAMENDIZ HERMEL ALFONSO'    	 ]);
		Teacher::create([
        	'name'   => 'CAICEDO RODRIGUEZ GILBERTO'    	 ]);
		Teacher::create([
        	'name'   => 'OSORIO SALAMANCA GINA PAOLA'    	 ]);
		Teacher::create([
        	'name'   => 'LOPEZ CABRERA OLGA LUCIA'    	 ]);
		Teacher::create([
        	'name'   => 'LUIZ CHAVEZ JULIO CESAR'    	 ]);
		Teacher::create([
        	'name'   => 'LUANGAS PATIÑO LUZ DARY'    	 ]);
		Teacher::create([
        	'name'   => 'HERNANDEZ RODRIGUEZ MARIA TERESA'    	 ]);
		Teacher::create([
        	'name'   => 'TOVAR TOSSE HENRY FABIAN'    	 ]);
		Teacher::create([
        	'name'   => 'PUENTES MUÑOZ MARCELA'    	 ]);
		Teacher::create([
        	'name'   => 'PUENTES MUÑOZ LYA MARCELA'    	 ]);
		Teacher::create([
        	'name'   => 'ANGARITA MOLINA GUZMAN OLMEDO'    	 ]);
		Teacher::create([
        	'name'   => 'DELGADO CAÑETE ANGELICA'    	 ]);
		Teacher::create([
        	'name'   => 'LEAL TERRANOVA OSCAR EDUARDO'    	 ]);
		Teacher::create([
        	'name'   => 'GALLEGO GUARIN OLMER'    	 ]);
		Teacher::create([
        	'name'   => 'VASQUEZ ORTEGA CRISTIAN ALBERTO'    	 ]);
		Teacher::create([
        	'name'   => 'HENAO VELASCO LUIS ALBERTO'    	 ]);
		Teacher::create([
        	'name'   => 'PAZ HURTADO ELVIZ'    	 ]);
		Teacher::create([
        	'name'   => 'HURTADO OSPINA DANIEL'    	 ]);
		Teacher::create([
        	'name'   => 'SERRANO MARIA EUGENIA'    	 ]);
		Teacher::create([
        	'name'   => 'CASTRO CLAVIJO JOSE NELSON'    	 ]);
		Teacher::create([
        	'name'   => 'CASTAÑEDA OVANDO ALEXANDRA'    	 ]);
		Teacher::create([
        	'name'   => 'MOSQUERA MOSQUERA JOSE DIOCLESIANO'    	 ]);
		Teacher::create([
        	'name'   => 'GUERRERO VILLAFAÑE JOSE FERNANDO'    	 ]);
		Teacher::create([
        	'name'   => 'TREJOS LOPEZ MARIA FERNANDA'    	 ]);
		Teacher::create([
        	'name'   => 'MARIÑO MONTAÑA JUAN SEBASTIAN'    	 ]);
		Teacher::create([
        	'name'   => 'LOIZA QUINTERO JULIO CESAR'    	 ]);
		Teacher::create([
        	'name'   => 'PRADA PRADA RUBEN DARIO '    	 ]);
		Teacher::create([
        	'name'   => 'ARBOLEDA MARTINE MONICA HELEN'    	 ]);
		Teacher::create([
        	'name'   => 'MONTERO MARTINEZ JESUS ANTONIO'    	 ]);
		Teacher::create([
        	'name'   => 'SEVILLA CHINCANGANA JIMMY ALBERTO'    	 ]);
		Teacher::create([
        	'name'   => 'GUZMAN CONTRERAS ANDRES MAURICIO'    	 ]);
		Teacher::create([
        	'name'   => 'ESCOBAR ALDANA GUILLERMO'    	 ]);
		Teacher::create([
        	'name'   => 'MICOLTA PADILLA CARMEN NILSA'    	 ]);
		Teacher::create([
        	'name'   => 'COLO CALDERON BELLANIRA'    	 ]);
		Teacher::create([
        	'name'   => 'BELALCAZAR GONGORA NOHEMY'    	 ]);
		Teacher::create([
        	'name'   => 'LUIZ CHAVEZ JULIO CESAR'    	 ]);
		Teacher::create([
        	'name'   => 'MEDINA CONZALES ANGEE ALEJANDRA'    	 ]);
		Teacher::create([
        	'name'   => 'QUIÑONES MENDEZ ORLANDO'    	 ]);
		Teacher::create([
        	'name'   => 'CONTRERAS DURAN DAYANIS VANESA'    	 ]);
		Teacher::create([
        	'name'   => 'HERNANDEZ PARRA CRISTOPHER ALBERTO'    	 ]);
		Teacher::create([
        	'name'   => 'RODRIGUEZ NEIZA LUIS CARLOS'    	 ]);
		Teacher::create([
        	'name'   => 'ROA ROMEO PAULA ANDREA'    	 ]);
		Teacher::create([
        	'name'   => 'RAMIREZ CORTES DANIELA AUGUSTO'    	 ]);
		Teacher::create([
        	'name'   => 'LOPEZ DELGADO EDWIN ORLANDO'    	 ]);
		Teacher::create([
        	'name'   => 'GUTIERREZ JIMENEZ OVIMER'    	 ]);
		Teacher::create([
        	'name'   => 'ROJAS PARRA JORGE EDY'    	 ]);
		Teacher::create([
        	'name'   => 'ZABALA GONZALES DANIEL'    	 ]);
        Teacher::create([
            'name'   => 'ZABALA SOTO JAIRO ANDRES'         ]);
		Teacher::create([
        	'name'   => 'DUQUE GARCIA LUZ ADRIANA'    	 ]);
		Teacher::create([
        	'name'   => 'MUNAR CASTELLANOS JENNIFES STEPHANE'    	 ]);
   		Teacher::create([
        	'name'   => 'PALOMINO TRUJILLO RUTH MARGARITA'    	 ]);
   		Teacher::create([
        	'name'   => 'LOZANO BARRIOS WILLIAN'    	 ]);
   		Teacher::create([
        	'name'   => 'FIGUEROA MARTINEZ WILLIAN ENRIQUE'    	 ]);
   		Teacher::create([
        	'name'   => 'ORJUELA GARZON MARIO FERNANDO'    	 ]);
   		Teacher::create([
        	'name'   => 'PINZON PIÑEROS MARTHA ELIZABETH'    	 ]);
   		Teacher::create([
        	'name'   => 'DIAZ PRADA MIGUEL ANGEL'    	 ]);
   		Teacher::create([
        	'name'   => 'RONQUILLO VALENCIA DAHIDU'    	 ]);
   		Teacher::create([
        	'name'   => 'ALVIS CAMELO CARLOS ANDRES'    	 ]);
   		Teacher::create([
        	'name'   => 'ZAPATA LOPERA CAROLINA'    	 ]);
   		Teacher::create([
        	'name'   => 'CORTES ROMERO IBETH DANELI'    	 ]);
   		Teacher::create([
        	'name'   => 'APARICIO GONZALEZ LIRA TATIANA'    	 ]);
   		Teacher::create([
        	'name'   => 'CRUZ SUAREZ DIANA MARCELA'    	 ]);
   		Teacher::create([
        	'name'   => 'CARDENAS ARIAS ANGELA DEL PILAR'    	 ]);
   		Teacher::create([
        	'name'   => 'HERNANDEZ CASTILLO CAMILA ANDREA'    	 ]);
   		Teacher::create([
        	'name'   => 'GIRALDO PEREZ SOFIA '    	 ]);
   		Teacher::create([
        	'name'   => 'CARDENAS FARFAN AMADEO'       	 ]);
   		Teacher::create([
        	'name'   => 'CORTES RODRIGUEZ INGRID ANDREA'       	 ]);
   		Teacher::create([
        	'name'   => 'MORENO CAICEDO JULIAN JAVIER'       	 ]);
   		Teacher::create([
        	'name'   => 'ARIZA ROJAS ROSALBA'       	 ]);
   		Teacher::create([
        	'name'   => 'CABALLERO GOMEZ ANDRES IVAN'       	 ]);
   		Teacher::create([
        	'name'   => 'GUEVARA NIÑO LEONEL'       	 ]);
   		Teacher::create([
        	'name'   => 'GIRALDO PEEZ SOFIA'       	 ]);
   		Teacher::create([
        	'name'   => 'BARAJAS PARAJA DORIS'       	 ]);
   		Teacher::create([
        	'name'   => 'RINCON ROJAS INGRID ASTRID'       	 ]);
 		Teacher::create([
        	'name'   => 'CASTILLO CARRANZA MARTHA LILIA'    	 ]);
        Teacher::create([
        	'name'   => 'GALINDO PIRAQUIVE JUAN CARLOS'    	 ]);
    	Teacher::create([
        	'name'   => 'HERNANDEZ VELASQUEZ ELAIDA'    	 ]);
    	Teacher::create([
        	'name'   => 'CELIS DURAN NOHORA ESPERANZA '    	 ]);
    	Teacher::create([
        	'name'   => 'BERMUDEZ GONZALEZ CARLOS EDUARDO'    	 ]);
        Teacher::create([
            'name'   => 'BERMUDEZ VARON NUBIA'       ]);
        Teacher::create([
            'name'   => 'BERMUDEZ GONZALEZ CARLOS EDUARDO'       ]);
        Teacher::create([
            'name'   => 'CASTAÑO FERNANDEZ BLANCA LUZ'       ]);
        Teacher::create([
            'name'   => 'CARRAL SANABRIA DIANA CONSTANCIA'       ]);
        Teacher::create([
            'name'   => 'LONDOÑO ZAPATA OSCAR IVAN'       ]);
        Teacher::create([
            'name'   => 'QUINTERO VARON ORLANDO'       ]);
        Teacher::create([
            'name'   => 'SILGADO RAMOS ALEX'       ]);
        Teacher::create([
            'name'   => 'MORENO AVILA GENTIL'       ]);
        Teacher::create([
            'name'   => 'VARGAS MOLINO NELSON MAURICIO'       ]);
        Teacher::create([
            'name'   => 'OSPITIA RAMIREZ ROBINSSON ALEXANDER'       ]);
         Teacher::create([
            'name'   => 'TOCORA LOZANO LEYDI MARCELA'       ]);
         Teacher::create([
            'name'   => 'ALARCON OROZCO ARNALDO'       ]);
         Teacher::create([
            'name'   => 'PARRAGA MORENO JUAN GABRIEL'       ]);
         Teacher::create([
            'name'   => 'AVILA TEJERO AMADOR'       ]);

}

}
