<?php

use Illuminate\Database\Seeder;
use App\Program;
use App\CalendarWeek;
class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Program::create([
        	'name'   => 'ADMINISTRACION FINANCIERA',
        	 ]);
        Program::create([
        	'name'   => 'ADMINISTRACION TURISTICA Y HOTELERA',
        	 ]);
        Program::create([
        	'name'   => 'INGENIERIA DE SISTEMAS',
        	 ]);
        Program::create([
        	'name'   => 'INGENIERÍA EN AGROECOLOGÍA',
        	 ]);
        Program::create([
        	'name'   => 'LIC. EDUCACION INFANTIL',
        	 ]);
        Program::create([
        	'name'   => 'LIC. EN CIENCIAS NATURALES Y EDUC. AMBTAL',
        	 ]);
        Program::create([
        	'name'   => 'LIC. EN EDUCACION ARTISTICA',
        	 ]);
        Program::create([
        	'name'   => 'LIC. EN LITERATURA Y LENGUA CASTELLANA',
        	 ]);
        Program::create([
        	'name'   => 'LIC. EN PEDAGOGIA INFANTIL',
        	 ]);
        Program::create([
        	'name'   => 'SALUD OCUPACIONAL',
        	]);
        Program::create([
        	'name'   => 'SEGURIDAD Y SALUD EN EL TRABAJO',
        	 ]);
         Program::create([
        	'name'   => 'TECN. EN GESTION DE BASES DE DATOS',
        	 ]);
         Program::create([
        	'name'   => 'TECN. EN PROTEC Y RECUP. DE ECOSISTEMAS FORESTALES',
        	 ]);
          Program::create([
        	'name'   => 'TECN. EN REGENCIA DE FARMACIA',
        	 ]);
          Program::create([
        	'name'   => 'LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',
        	 ]);
          CalendarWeek::create([
            'init_date'      =>  '2020-03-02',
            'end_date'		 =>  '2020-06-15',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
        	]);
          CalendarWeek::create([
            'init_date'      =>  '2020-03-03',
            'end_date'		 =>  '2020-06-16',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
       	 ]);
          CalendarWeek::create([
            'init_date'      =>  '2020-03-04',
            'end_date'		 =>  '2020-06-17',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
        ]); CalendarWeek::create([
            'init_date'      =>  '2020-03-05',
            'end_date'		 =>  '2020-06-18',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
        ]); CalendarWeek::create([
            'init_date'      =>  '2020-03-06',
            'end_date'		 =>  '2020-06-19',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-07',
            'end_date'		 =>  '2020-06-20',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-08',
            'end_date'		 =>  '2020-06-21',
            'nweek'			=> 	'8',
            'week'			=> 	'1',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-09',
            'end_date'		 =>  '2020-06-22',
            'nweek'			=> 	'8',
            'week'			=> 	'2',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-10',
            'end_date'		 =>  '2020-06-23',
            'nweek'			=> 	'8',
            'week'			=> 	'2',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-11',
            'end_date'		 =>  '2020-06-24',
            'nweek'			=> 	'8',
            'week'			=> 	'2',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-12',
            'end_date'		 =>  '2020-06-25',
            'nweek'			=> 	'8',
            'week'			=> 	'2',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-13',
            'end_date'		 =>  '2020-06-26',
            'nweek'			=> 	'8',
            'week'			=> 	'2',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-14',
            'end_date'		 =>  '2020-06-27',
            'nweek'			=> 	'8',
            'week'			=> 	'2',
        ]);CalendarWeek::create([
            'init_date'      =>  '2020-03-15',
            'end_date'		 =>  '2020-06-28',
            'nweek'			=> 	'8',
            'week'			=> 	'2', ]);
    }
}
