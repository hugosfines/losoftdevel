<?php

use Illuminate\Database\Seeder;
use App\CourProgram;
class CourProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         CourProgram::create([
           'program_id'=>'3',
            'cour_id'=>  '190',
            'level_id'=> '8',
            'name'=>        'INGENIERIA DE NEGOCIOS DE INGENIERIA DE SISTEMAS',
            ]);CourProgram::create([
         	 'program_id'=>          '3',
          	 'cour_id'=>             '1',
           	 'level_id'=>            '8',
          	'name'=>        'SEGURIDAD DE LA INFORMACION DE INGENIERIA DE SISTEMAS']);
        CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '6',
        		'level_id'=>            '8',
       		 	'name'=>        'ECONOMIA Y PRINCIPIOS FINANCIEROS DE INGENIERIA DE SISTEMAS',]);
        CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '129',
        		'level_id'=>            '8',
       		 	'name'=>        'MODELOS Y SIMULACION DE INGENIERIA DE SISTEMAS',]);
        CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '168',
        		'level_id'=>            '8',
       		 	'name'=>        'ELECTIVA PROFESIONAL I DE INGENIERIA DE SISTEMAS',]);
        CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '167',
        		'level_id'=>            '8',
       		 	'name'=> 'ELECTIVA PROFESIONAL II - SISTEMAS DE CONTROL DE INFORMACION DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '169',
        		'level_id'=>            '9',
       		 	'name'=> 'ELECTIVA PROFESIONAL III - INFORMATICA FORENSE DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '207',
        		'level_id'=>            '9',
       		 	'name'=> 'ADMINISTRACION DE TALENTO HUMANO DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '129',
        		'level_id'=>            '7',
       		 	'name'=> 'MODELOS DE CONOCIMIENTO DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '127',
        		'level_id'=>            '7',
       		 	'name'=> 'AUDITORIA Y LEGISLACION INFORMATICA DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '133',
        		'level_id'=>            '7',
       		 	'name'=> 'INVESTIGACION DE OPERACIONES DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '126',
        		'level_id'=>            '7',
       		 	'name'=> 'PROGRAMACION DE SISTEMAS INTELIGENTES DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '405',
        		'level_id'=>            '7',
       		 	'name'=> 'MATEMATICAS DISCRETAS II DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '404',
        		'level_id'=>            '7',
       		 	'name'=> 'METODOS NUMERICOS DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '359',
        		'level_id'=>            '6',
       		 	'name'=> 'PROBABILIDAD II DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '181',
        		'level_id'=>            '6',
       		 	'name'=> 'ECUACIONES DIFERENCIALES DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '152',
        		'level_id'=>            '6',
       		 	'name'=> 'REDES NEURONALES DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '407',
        		'level_id'=>            '6',
       		 	'name'=> 'COMUNICACION DEL INGLES DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '409',
        		'level_id'=>            '6',
       		 	'name'=> 'ELECTIVA HUMANIDADES - RECREACION Y DEPORTE DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '3',
      	   		'cour_id'=>             '154',
        		'level_id'=>            '6',
       		 	'name'=> 'MINERIA DE DATOS DE INGENIERIA DE SISTEMAS',]);
       	CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '4',
        		'level_id'=>            '1',
       		 	'name'=> 'FUNDAMENTOS DE ECONOMIA Y MICRO DE ADMINISTRACION FINANCIERA',]);
       	CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '37',
        		'level_id'=>            '5',
       		 	'name'=> 'ANALISIS FINANCIERO DE ADMINISTRACION FINANCIERA',]);
       	CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '148',
        		'level_id'=>            '1',
       		 	'name'=> 'FUNDAMENTOS DE PENSAMIENTO HUMANO DE ADMINISTRACION FINANCIERA',]);
       	CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '42',
        		'level_id'=>            '8',
       		 	'name'=> 'AUDITORIA FINANCIERA DE ADMINISTRACION FINANCIERA',]);
       	CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '7',
        		'level_id'=>            '1',
       		 	'name'=> 'SEMINARIO DE AUTOFORMACION DE ADMINISTRACION FINANCIERA',]);
       	CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '8',
        		'level_id'=>            '6',
       		 	'name'=> 'ADMINISTRACION DE LA PRODUCCION Y OPERACIONES DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '9',
        		'level_id'=>            '1',
       		 	'name'=> 'FUNDAMENTOS DE MATEMATICAS DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '10',
        		'level_id'=>            '3',
       		 	'name'=> 'ALGEBRA LINEAL DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '99',
        		'level_id'=>            '2',
       		 	'name'=> 'MACROECONOMIA DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '110',
        		'level_id'=>            '6',
       		 	'name'=> 'PRESUPUESTO DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '100',
        		'level_id'=>            '2',
       		 	'name'=> 'ORGANIZACIONES DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '69',
        		'level_id'=>            '2',
       		 	'name'=> 'CALCULO UNIVARIADO DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>          '1',
      	   		'cour_id'=>             '108',
        		'level_id'=>            '6',
       		 	'name'=> 'INGLES DE NEGOCIOS I DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '106',
        		'level_id'=>            '4',
       		 	'name'=> 'MATEMATICAS FINANCIERA II DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '12',
        		'level_id'=>            '3',
       		 	'name'=> 'MATEMATICAS FINANCIERA I DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '104',
        		'level_id'=>            '3',
       		 	'name'=> 'FORMACIÓN DEPORTIVA Y ARTE DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '81',
        		'level_id'=>            '2',
       		 	'name'=> 'COMPETENCIAS COMUNICATIVAS- COGNITIVAS- LECTURA Y ESCRITURA DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '13',
        		'level_id'=>            '3',
       		 	'name'=> 'CONTABILIDAD FINANCIERA DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '111',
        		'level_id'=>            '3',
       		 	'name'=> 'ECONOMETRIA BASICA DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '101',
        		'level_id'=>            '2',
       		 	'name'=> 'FUNDAMENTOS DE CONTABILIDAD DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '102',
        		'level_id'=>            '3',
       		 	'name'=> 'INFORMATICA Y METODOS CUANTITATIVOS DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '103',
        		'level_id'=>            '4',
       		 	'name'=> 'COSTOS DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '105',
        		'level_id'=>            '4',
       		 	'name'=> 'ESTADISTICA BASICA DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '107',
        		'level_id'=>            '4',
       		 	'name'=> 'FUNDAMENTOS DE MERCADEO DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '316',
        		'level_id'=>            '10',
       		 	'name'=> 'GERENCIA FINANCIERA DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '408',
        		'level_id'=>            '10',
       		 	'name'=> 'RESPONSABILIDAD SOCIAL EMPRESARIAL DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '379',
        		'level_id'=>            '10',
       		 	'name'=> 'POLITICA DE NEGOCIOS DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	  'program_id' =>             '1',
      	   	 'cour_id'=>             '109',
        	 'level_id'=>            '6',
       		 'name'=> 'OPTATIVA - MICROFINANZAS DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '1',
      	   		'cour_id'=>             '376',
        		'level_id'=>            '10',
       		 	'name'=> 'EVALUACION Y GERENCIA DE PROYECTOS DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '14',
        		'level_id'=>            '1',
       		 	'name'=> 'SEMINARIO DE PERMANENTE PARA LA AUTOFORMACION DE ADMINISTRACION FINANCIERA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '15',
        		'level_id'=>            '1',
       		 	'name'=> 'BIOLOGIA CELULAR Y MICROBIOLOGIA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '35',
        		'level_id'=>            '4',
       		 	'name'=> 'FARMACOLOGIA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '294',
        		'level_id'=>            '2',
       		 	'name'=> 'MATEMATICAS APLICADA Y ESTADISTICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '121',
        		'level_id'=>            '4',
       		 	'name'=> 'CONTABILIDAD Y PRESUPUESTOS DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '39',
        		'level_id'=>            '4',
       		 	'name'=> 'GERENCIA ESTRATEGICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '17',
        		'level_id'=>            '1',
       		 	'name'=> 'INFORMATICA Y TELEMATICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '21',
        		'level_id'=>            '1',
       		 	'name'=> 'TECNICAS DE COMUNICACION ORAL Y ESCRITA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '22',
        		'level_id'=>            '2',
       		 	'name'=> 'QUÍMICA GENERAL E INORGANICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '23',
        		'level_id'=>            '2',
       		 	'name'=> 'MATEMÁTICA APLICADA Y ESTADISTICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '24',
        		'level_id'=>            '2',
       		 	'name'=> 'SOCIO ANTROPOLOGIA DE LA SALUD DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '25',
        		'level_id'=>            '2',
       		 	'name'=> 'TECNOLOGIA EN FARMACIA I DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '2',
      	   		'cour_id'=>             '367',
        		'level_id'=>            '9',
       		 	'name'=> 'LEGISLACION EMPRESARIAL DE ADMINISTRACION TURISTICA Y HOTELERA',]);
       		CourProgram::create([
          	    'program_id' =>             '2',
      	   		'cour_id'=>             '380',
        		'level_id'=>            '9',
       		 	'name'=> 'POLITICA Y PLANEACION DEL TURISMO DE ADMINISTRACION TURISTICA Y HOTELERA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '40',
        		'level_id'=>            '6',
       		 	'name'=> 'FARMACIA VETERINARIA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '43',
        		'level_id'=>            '6',
       		 	'name'=> 'ETICA Y BIOETICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '44',
        		'level_id'=>            '6',
       		 	'name'=> 'PRACTICAS EN REGENCIA DE FARMACIA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '41',
        		'level_id'=>            '6',
       		 	'name'=> 'FARMACIA MAGISTRAL DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '29',
        		'level_id'=>            '3',
       		 	'name'=> 'MORFOFISIOLOGIA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '30',
        		'level_id'=>            '3',
       		 	'name'=> 'ELECTIVA: FARMACO - EPIDEMIOLOGÍA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '118',
        		'level_id'=>            '3',
       		 	'name'=> 'ADMINISTRACION DE FARMACIA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '249',
        		'level_id'=>            '3',
       		 	'name'=> 'BIOQUIMICA DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '36',
        		'level_id'=>            '4',
       		 	'name'=> 'FUNDAMENTOS DE INVESTIGACION DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '2',
        		'level_id'=>            '3',
       		 	'name'=> 'ELECTIVA: TERAPIAS ALTERNATIVAS Y COMPLEMENTARIAS DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '14',
      	   		'cour_id'=>             '112',
        		'level_id'=>            '1',
       		 	'name'=> 'INGLES I DE TEC. EN REGENCIA DE FARMACIA',]);
       		CourProgram::create([
          	    'program_id' =>             '5',
      	   		'cour_id'=>             '44',
        		'level_id'=>            '2',
       		 	'name'=> 'COMPETENCIAS COMUNICATIVAS II DE LIC. EDUCACION INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '5',
      	   		'cour_id'=>             '46',
        		'level_id'=>            '2',
       		 	'name'=> 'ATENCION INTEGRAL EN SALUD A LA PRIMERA INFANCIA DE LIC. EDUCACION INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '5',
      	   		'cour_id'=>             '47',
        		'level_id'=>            '2',
       		 	'name'=> 'PRACTICA II: DIDACTICA INFANCIA TEMPRANA (1 A 3 AÑOS) DE LIC. EDUCACION INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '5',
      	   		'cour_id'=>             '48',
        		'level_id'=>            '3',
       		 	'name'=> 'PRACTICA III: DIDACTICA EDAD PREESCOLAR (3 A 4 AÑOS) DE LIC. EDUCACION INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '5',
      	   		'cour_id'=>             '49',
        		'level_id'=>            '3',
       		 	'name'=> 'SEGURIDAD ALIMENTARIA NUTRICIONAL DE LIC. EDUCACION INFANTIL',]);
       	CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '52',
        		'level_id'=>            '6',
       		 	'name'=> 'ELECTIVA - LA INFORMATICA EN LA FORMACION INTEGRAL - PLAN IV - DE LIC. EN PEDAGOGIA INFANTIL',]);
       	CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '82',
        		'level_id'=>            '10',
       		 	'name'=> 'INVESTIGACION Y PRACTICA X: LAS TIC EN EDUCACION DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '70',
        		'level_id'=>            '6',
       		 	'name'=> 'EL INGLES COMO SEGUNDA LENGUA DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '53',
        		'level_id'=>            '7',
       		 	'name'=> 'PROCESOS DE LECTURA Y ESCRITURA DEL NIÑO DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '55',
        		'level_id'=>            '7',
       		 	'name'=> 'INVESTIGACION Y PRACTICA VII: PROSPECTIVA DEL PEDAGOGO INTEGRAL PARA EL PREESCOLAR DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '71',
        		'level_id'=>            '6',
       		 	'name'=> 'INVESTIGACION Y PRACTICA VI: CURRICULO PARA PREESCOLAR DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '72',
        		'level_id'=>            '6',
       		 	'name'=> 'DISEÑO DEL PROYECTO DE INTERVENCION PEDAGOGICA DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '56',
        		'level_id'=>            '7',
       		 	'name'=> 'FILOSOFIA Y EDUCACION DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '73',
        		'level_id'=>            '8',
       		 	'name'=> 'EVALUACION DE LOS PROCESOS EDUCATIVOS DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '75',
        		'level_id'=>            '8',
       		 	'name'=> 'LINEAMIENTOS DE LA BASICA PRIMARIA DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '76',
        		'level_id'=>            '9',
       		 	'name'=> 'ANTROPOLOGIA Y EDUCACION DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '77',
        		'level_id'=>            '9',
       		 	'name'=> 'CONSTITUCION POLITICA DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '78',
        		'level_id'=>            '9',
       		 	'name'=> 'INVESTIGACION Y PRACTICA IX: NECESIDADES EDUCATIVAS ESPECIALES DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '79',
        		'level_id'=>            '10',
       		 	'name'=> 'PROYECTO DE INVESTIGACION FORMATIVA II : INTERVENCION PEDAGOGICA DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '80',
        		'level_id'=>            '10',
       		 	'name'=> 'PROYECTOS EDUCATIVOS ESPECIALES DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '9',
      	   		'cour_id'=>             '74',
        		'level_id'=>            '8',
       		 	'name'=> 'INVESTIGACION Y PRACTICA VIII: ARTICULACION PREESCOLAR Y PRIMERO DE PRIMARIA DE LIC. EN PEDAGOGIA INFANTIL',]);
       		CourProgram::create([
          	    'program_id' =>             '15',
      	   		'cour_id'=>             '82',
        		'level_id'=>            '7',
       		 	'name'=> 'EL ESTUDIO DE LOS ACTOS COMUNICATIVOS DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       		CourProgram::create([
          	    'program_id' =>             '15',
      	   		'cour_id'=>             '83',
        		'level_id'=>            '7',
       		 	'name'=> 'PROYECTO DE INVESTIGACION FORMATIVA: PED. DE LA LITERATURA DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       		CourProgram::create([
          	    'program_id' =>             '15',
      	   		'cour_id'=>             '85',
        		'level_id'=>            '7',
       		 	'name'=> 'EVALUACION POR PROCESOS DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       		CourProgram::create([
          	    'program_id' =>             '15',
      	   		'cour_id'=>             '86',
        		'level_id'=>            '7',
       		 	'name'=> 'ELECTIVA DE VII DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '89',
        	'level_id'=>            '8',
       		'name'=> 'DESARROLLO DE COMPETENCIAS A TRAVES DEL LENGUAJE DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '90',
        	'level_id'=>            '8',
       		'name'=> 'PEDAGOGIA PSICOSOCIAL DEL LENGUAJE DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '90',
        	'level_id'=>            '8',
       		'name'=> 'ELECTIVA DE VIII DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '92',
        	'level_id'=>            '8',
       		'name'=> 'PEDAGOGIA DE LA LITERATURA INFANTIL DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '93',
        	'level_id'=>            '9',
       		'name'=> 'ELECTIVA DE IX DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '94',
        	'level_id'=>            '9',
       		'name'=> 'LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '95',
        	'level_id'=>            '9',
       		'name'=> 'PRODUCCION Y RECEPCION TEXTUAL INTERACTIVA DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
       	CourProgram::create([
          	'program_id' =>         '15',
      	   	'cour_id'=>             '96',
        	'level_id'=>            '9',
       		'name'=> 'PEDAGOGIA Y SOCIEDAD EN COLOMBIA DE LIC. EN EDUCACION BASICA CON ENFASIS EN LENGUA CASTELLANA',]);
    }
}
