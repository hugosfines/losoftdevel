<?php

use Illuminate\Database\Seeder;
use App\CatWeekday;
class CatWeekdaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '1',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '3',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '13',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '22',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '22',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '2',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '8',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '20',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '11',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '18',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '6',
        	 ]);CatWeekday::create([
            'weekday'   => '6',
            'status'    => '1',
            'cat_id'    => '6',
             ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '14',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '21',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '4',
        	 ]);CatWeekday::create([
            'weekday'   => '6',
            'status'    => '1',
            'cat_id'    => '4',
             ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '5',
        	 ]);CatWeekday::create([
            'weekday'   => '6',
            'status'    => '1',
            'cat_id'    => '5',
             ]);CatWeekday::create([
        	'weekday'   => '1',
        	'status' 	=> '1',
        	'cat_id' 	=> '5',
        	 ]);CatWeekday::create([
        	'weekday'   => '1',
        	'status' 	=> '1',
        	'cat_id' 	=> '6',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '19',
        	 ]);CatWeekday::create([
            'weekday'   => '6',
            'status'    => '1',
            'cat_id'    => '19',
             ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '10',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '15',
        	 ]);CatWeekday::create([
        	'weekday'   => '6',
        	'status' 	=> '1',
        	'cat_id' 	=> '10',
        	 ]);CatWeekday::create([
        	'weekday'   => '1',
        	'status' 	=> '1',
        	'cat_id' 	=> '8',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '8',
        	 ]);CatWeekday::create([
        	'weekday'   => '2',
        	'status' 	=> '1',
        	'cat_id' 	=> '13',
        	 ]);CatWeekday::create([
        	'weekday'   => '1',
        	'status' 	=> '1',
        	'cat_id' 	=> '12',
        	 ]);CatWeekday::create([
        	'weekday'   => '1',
        	'status' 	=> '1',
        	'cat_id' 	=> '13',
        	 ]);CatWeekday::create([
        	'weekday'   => '7',
        	'status' 	=> '1',
        	'cat_id' 	=> '23',
        	 ]);CatWeekday::create([
            'weekday'   => '6',
            'status'    => '1',
            'cat_id'    => '16',
             ]);CatWeekday::create([
            'weekday'   => '6',
            'status'    => '1',
            'cat_id'    => '17',
            ]);
    }
}
