<?php

use Illuminate\Database\Seeder;
use App\Group;
class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'name'      => '1',
            'cour_program_id' => '23',
            'teacher_id' => '10',
            'classroom_id' => '1',]);
        Group::create([
            'name'      => '1',
            'cour_program_id' => '21',
            'teacher_id' => '1',
            'classroom_id' => '2',]);

    }
}
