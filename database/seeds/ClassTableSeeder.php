<?php

use Illuminate\Database\Seeder;
use App\Lesson;
class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '1',
            'datetime' => '2020-03-07 10:00:00',
            'group_id'=> '1',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '2',
            'datetime' => '2020-03-21 10:00:00',
            'group_id'=> '1',]);
       Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '3',
            'datetime' => '2020-04-04 10:00:00',
            'group_id'=> '1',]);
       Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '4',
            'datetime' => '2020-04-25 10:00:00',
            'group_id'=> '1',]);
       Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '5',
            'datetime' => '2020-05-09 10:00:00',
            'group_id'=> '1',]);
       Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '6',
            'datetime' => '2020-05-23 10:00:00',
            'group_id'=> '1',]);
        Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '7',
            'datetime' => '2020-06-06 10:00:00',
            'group_id'=> '1',]);
        Lesson::create([
            'name'      => 'FUNDAMENTOS DE PENSAMIENTO HUMANO',
            'week' => '1',
            'tutorial' => '8',
            'datetime' => '2020-06-20 10:00:00',
            'group_id'=> '1',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '1',
            'datetime' => '2020-03-07 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '2',
            'datetime' => '2020-03-21 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '3',
            'datetime' => '2020-04-04 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '4',
            'datetime' => '2020-04-25 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '5',
            'datetime' => '2020-05-09 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '6',
            'datetime' => '2020-05-23 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '7',
            'datetime' => '2020-06-06 07:00:00',
            'group_id'=> '2',]);
      Lesson::create([
            'name'      => 'FUNDAMENTOS DE ECONOMIA Y MICRO',
            'week' => '1',
            'tutorial' => '8',
            'datetime' => '2020-06-20 07:00:00',
            'group_id'=> '2',]);
    }
}
