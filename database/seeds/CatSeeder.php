<?php

use Illuminate\Database\Seeder;
use App\Cat;
use App\Level;

class CatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cat::create([
        	'name'   => 'BARRANQUILLA',
        	'activo' =>	'SI',
        	 ]);
         Cat::create([
        	'name'   => 'CAJAMARCA',
        	'activo' =>	'SI',
        	 ]);

         Cat::create([
        	'name'   => 'CALI',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'CHAPARRAL',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'GIRARDOT',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'HONDA',
        	'activo' =>	'SI',
        	 ]);

        $ibague_cat = Cat::create([
            'name'   => 'IBAGUE',
            'activo' => 'SI',
             ]);


        Cat::create([
        	'name'   => 'KENNEDY',
        	'activo' =>	'SI',
        	 ]);
         Cat::create([
            'name'   => 'LIBANO',
            'activo' => 'SI',
             ]);

         Cat::create([
        	'name'   => 'MEDELLIN',
        	'activo' =>	'SI',
        	 ]);

         Cat::create([
        	'name'   => 'MELGAR',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'MOCOA',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'NEIVA',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'PACHO',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'PEREIRA',
        	'activo' =>	'SI',
        	 ]);

        Cat::create([
        	'name'   => 'PLANADAS',
        	'activo' =>	'SI',
        	 ]);

		Cat::create([
        	'name'   => 'POPAYAN',
        	'activo' =>	'SI',
        	 ]);

		Cat::create([
        	'name'   => 'PURIFICACION',
        	'activo' =>	'SI',
        	 ]);

		Cat::create([
        	'name'   => 'RIO BLANCO',
        	'activo' =>	'SI',
        	 ]);

		Cat::create([
        	'name'   => 'SIBATE',
        	'activo' =>	'SI',
        	 ]);

		Cat::create([
        	'name'   => 'SUBA',
        	'activo' =>	'SI',
        	 ]);

		Cat::create([
        	'name'   => 'TUNAL',
        	'activo' =>	'SI',
        	 ]);

		$uraba_cat = Cat::create([
        	'name'   => 'URABA',
        	'activo' =>	'SI',
        	 ]);

       foreach (range(1,7) as $day) {
            $ibague_cat->weekdays()->create([
               'weekday' => $day,
                'status'  => 1,
           ]);//este codigo es cuando el departamento tiene todos lo dias clases
       }

		DB::table('levels')->insert([
            'name'      =>  'I',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'II',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'III',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'IV',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'V',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'VI',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'VII',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'VIII',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'IX',
        ]);
        DB::table('levels')->insert([
            'name'      =>  'X',
        ]);
    }
}
