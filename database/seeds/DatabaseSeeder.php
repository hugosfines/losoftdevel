<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(CatSeeder::class);
        $this->call(CourSeeder::class);$this->call(CatWeekdaySeeder::class);
        $this->call(ProgramSeeder::class);$this->call(CourProgramSeeder::class);
        $this->call(TeacherSeeder::class); $this->call(CatProgramSeeder::class);
        $this->call(GroupTableSeeder::class);$this->call(ClassTableSeeder::class);
    }
}

