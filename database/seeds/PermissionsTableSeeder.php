<?php

use Illuminate\Database\Seeder;

use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Users
        Permission::create([
            'name'          => 'Navegar usuarios',
            'slug'          => 'users.index',
            'description'   => 'Lista y navega todos los usuarios del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de usuario',
            'slug'          => 'users.show',
            'description'   => 'Ve en detalle cada usuario del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Edición de usuarios',
            'slug'          => 'users.edit',
            'description'   => 'Podría editar cualquier dato de un usuario del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar usuario',
            'slug'          => 'users.destroy',
            'description'   => 'Podría eliminar cualquier usuario del sistema',      
        ]);

        //Roles
        Permission::create([
            'name'          => 'Navegar roles',
            'slug'          => 'roles.index',
            'description'   => 'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de un rol',
            'slug'          => 'roles.show',
            'description'   => 'Ve en detalle cada rol del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de roles',
            'slug'          => 'roles.create',
            'description'   => 'Podría crear nuevos roles en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de roles',
            'slug'          => 'roles.edit',
            'description'   => 'Podría editar cualquier dato de un rol del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar roles',
            'slug'          => 'roles.destroy',
            'description'   => 'Podría eliminar cualquier rol del sistema',      
        ]);

       Permission::create([
            'name'          => 'Navegar log',
            'slug'          => 'logs.index',
            'description'   => 'Lista y navega el log del sistema',
        ]);


        /**CAT*/
        Permission::create([
            'name'          => 'Navegar CAT',
            'slug'          => 'cat.index',
            'description'   => 'Lista y navega todos los CAT',
        ]);
        
        Permission::create([
            'name'          => 'Creación de CAT',
            'slug'          => 'cat.create',
            'description'   => 'Crear nuevos CAT en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de CAT',
            'slug'          => 'cat.edit',
            'description'   => 'Editar cualquier dato de un CAT',
        ]);

        /**CLASSROOMS*/
        Permission::create([
            'name'          => 'Navegar salones',
            'slug'          => 'classrooms.index',
            'description'   => 'Lista y navega todos los salones',
        ]);
        
        Permission::create([
            'name'          => 'Creación de salones',
            'slug'          => 'classrooms.create',
            'description'   => 'Crear nuevos salones en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de salones',
            'slug'          => 'classrooms.edit',
            'description'   => 'Editar cualquier dato de un salones',
        ]);

        //attendances
        Permission::create([
            'name'          => 'Ver detalle de cada asistencia',
            'slug'          => 'attendances.show',
            'description'   => 'Ver en detalle de cada asistencia del sistema',
        ]);
        

        Permission::create([
            'name'          => 'Navegar asistencia',
            'slug'          => 'attendances.index',
            'description'   => 'Lista y navega todos los excusas',
        ]);

        Permission::create([
            'name'          => 'Creación de asistencia',
            'slug'          => 'attendances.create',
            'description'   => 'Crear nuevos asistencia en el sistema',
        ]); 

        Permission::create([
            'name'      => 'Editar asistencia',
            'slug'      => 'attendances.edit',
            'description'=> 'Editar cada detalle del modulo de asistencia del sistema',
        ]);

         /**EXCUSES*/
         Permission::create([
            'name'      => 'Ver excusas',
            'slug'      => 'excuses.show',
            'description'=> 'Ver cada detalle de cada excusas del sistema',
        ]); 
         
        Permission::create([
            'name'          => 'Navegar excusas',
            'slug'          => 'excuses.index',
            'description'   => 'Lista y navega todos los excusas',
        ]);
        
        Permission::create([
            'name'          => 'Subir excusas',
            'slug'          => 'excuses.create',
            'description'   => 'Subir una excusa',
        ]);
        //teachers 
        Permission::create([
            'name'          => 'Navegar docente',
            'slug'          => 'teachers.index',
            'description'   => 'Lista y navega todos los docente',
        ]);

        Permission::create([
            'name'          => 'Creación de docentes',
            'slug'          => 'teachers.create',
            'description'   => 'Crear nuevos docentes en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Editar docentes',
            'slug'          => 'teachers.edit',
            'description'   => 'Editar datos de un docente del sistema',
        ]);
        //programs 
        Permission::create([
            'name'          => 'Navegar programas',
            'slug'          => 'programs.index',
            'description'   => 'Lista y navega todos los programas ',
        ]);

        Permission::create([
            'name'          => 'Creación de programas ',
            'slug'          => 'programs.create',
            'description'   => 'Crear nuevos programas en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Editar programas',
            'slug'          => 'programs.edit',
            'description'   => 'Editar datos de un programas del sistema',
        ]);

         
        //programs 
        Permission::create([
            'name'          => 'Navegar curso',
            'slug'          => 'cours.index',
            'description'   => 'Lista y navega todos los curso ',
        ]);

        Permission::create([
            'name'          => 'Creación de curso',
            'slug'          => 'cours.create',
            'description'   => 'Crear nuevos curso en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Editar curso',
            'slug'          => 'cours.edit',
            'description'   => 'Editar datos de un curso del sistema',
        ]);

         Permission::create([
            'name'          => 'Navegar reportes',
            'slug'          => 'reports.index',
            'description'   => 'Lista y navega todos los usuarios'
        ]);

        Permission::create([
            'name'          => 'Ver detalle de reporte ',
            'slug'          => 'reports.show',
            'description'   => 'Ver en detalle de cada reporte del sistema'
        ]);

        Permission::create([
            'name'          => 'Creación de reportes',
            'slug'          => 'reports.create',
            'description'   => 'Crear nuevos reportes'
        ]);
        
        Permission::create([
            'name'          => 'Edición de reportes',
            'slug'          => 'reports.edit',
            'description'   => 'Editar los datos de cualquier reporte',
        ]);

        //Usario administrador
        DB::table('users')->insert([
            'name'      =>  'administrador',
            'email'     =>  'admin@admin.com',
            'password'  =>  bcrypt('administrador'),
        ]);

        //Rol admin
        Role::create([
            'name'          => 'Administrador',
            'slug'          => 'Admin',
            'description'   => 'Usuario con acceso total',
            'special'       => 'all-access'
        ]);

        DB::table('users')->insert([
            'name'      =>  'monitor',
            'email'     =>  'moni@moni.com',
            'password'  =>  bcrypt('monitor'),
        ]);
        //Rol admin
        Role::create([
            'name'          => 'Monitor',
            'slug'          => 'Moni',
            'description'   => 'Usuario con acceso al control de asistencia',
            
        ]);

        //Asignacion del permiso al admin
        DB::table('role_user')->insert([
            'role_id' => '1',   
            'user_id' => '1',
        ]);
        DB::table('role_user')->insert([
            'role_id' => '2',   
            'user_id' => '2',
        ]);
        DB::table('permission_role')->insert([
            'role_id' => '2',   
            'permission_id' => '17',
        ]);
        DB::table('permission_role')->insert([
            'role_id' => '2',   
            'permission_id' => '18',
        ]);
        DB::table('permission_role')->insert([
            'role_id' => '2',   
            'permission_id' => '19',
        ]);
        DB::table('permission_role')->insert([
            'role_id' => '2',   
            'permission_id' => '20',
        ]);
        //   DB::table('role_user')->insert([
        //     'role_id' => '2',   
        //     'user_id' => '5'
        // ]);
        //    DB::table('role_user')->insert([
        //     'role_id' => '2',   
        //     'user_id' => '3',
        // ]);
    }
}
