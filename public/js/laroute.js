(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":"password.update","action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"email\/verify","name":"verification.notice","action":"App\Http\Controllers\Auth\VerificationController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"email\/verify\/{id}","name":"verification.verify","action":"App\Http\Controllers\Auth\VerificationController@verify"},{"host":null,"methods":["GET","HEAD"],"uri":"email\/resend","name":"verification.resend","action":"App\Http\Controllers\Auth\VerificationController@resend"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":"home","action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"users","name":"users.index","action":"App\Http\Controllers\UserController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/create","name":"users.create","action":"App\Http\Controllers\UserController@create"},{"host":null,"methods":["POST"],"uri":"users\/store","name":"users.store","action":"App\Http\Controllers\UserController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/{user}\/edit","name":"users.edit","action":"App\Http\Controllers\UserController@edit"},{"host":null,"methods":["PUT"],"uri":"users\/{user}","name":"users.update","action":"App\Http\Controllers\UserController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"roles","name":"roles.index","action":"App\Http\Controllers\RoleController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"roles\/create","name":"roles.create","action":"App\Http\Controllers\RoleController@create"},{"host":null,"methods":["POST"],"uri":"roles\/store","name":"roles.store","action":"App\Http\Controllers\RoleController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"roles\/{role}\/edit","name":"roles.edit","action":"App\Http\Controllers\RoleController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"roles\/{role}","name":"roles.show","action":"App\Http\Controllers\RoleController@show"},{"host":null,"methods":["PUT"],"uri":"roles\/{role}","name":"roles.update","action":"App\Http\Controllers\RoleController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"logs","name":"logs.index","action":"App\Http\Controllers\LogController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"cats","name":"cats.index","action":"App\Http\Controllers\CatController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"cats\/create","name":"cats.create","action":"App\Http\Controllers\CatController@create"},{"host":null,"methods":["POST"],"uri":"cats\/store","name":"cats.store","action":"App\Http\Controllers\CatController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"cats\/{cat}\/edit","name":"cats.edit","action":"App\Http\Controllers\CatController@edit"},{"host":null,"methods":["PUT"],"uri":"cats\/{cat}","name":"cats.update","action":"App\Http\Controllers\CatController@update"},{"host":null,"methods":["POST"],"uri":"classrooms\/list","name":"classrooms.list","action":"App\Http\Controllers\ClassroomController@classroomByCat"},{"host":null,"methods":["GET","HEAD"],"uri":"classrooms","name":"classrooms.index","action":"App\Http\Controllers\ClassroomController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"classrooms\/create","name":"classrooms.create","action":"App\Http\Controllers\ClassroomController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"classrooms\/show","name":"classrooms.show","action":"App\Http\Controllers\ClassroomController@show"},{"host":null,"methods":["POST"],"uri":"classrooms\/store","name":"classrooms.store","action":"App\Http\Controllers\ClassroomController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"classrooms\/{classroom}\/edit","name":"classrooms.edit","action":"App\Http\Controllers\ClassroomController@edit"},{"host":null,"methods":["PUT"],"uri":"classrooms\/{classroom}","name":"classrooms.update","action":"App\Http\Controllers\ClassroomController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"attendances","name":"attendances.index","action":"App\Http\Controllers\AttendanceController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"attendances\/create","name":"attendances.create","action":"App\Http\Controllers\AttendanceController@create"},{"host":null,"methods":["POST"],"uri":"attendances\/store","name":"attendances.store","action":"App\Http\Controllers\AttendanceController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"attendances\/{attendance}\/edit","name":"attendances.edit","action":"App\Http\Controllers\AttendanceController@edit"},{"host":null,"methods":["GET","HEAD"],"uri":"attendances\/download","name":"attendances.download","action":"App\Http\Controllers\AttendanceController@download"},{"host":null,"methods":["GET","HEAD"],"uri":"attendances\/{attendance}","name":"attendances.show","action":"App\Http\Controllers\AttendanceController@show"},{"host":null,"methods":["PUT"],"uri":"attendances\/{attendance}","name":"attendances.update","action":"App\Http\Controllers\AttendanceController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"programs","name":"programs.index","action":"App\Http\Controllers\ProgramController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"programs\/create","name":"programs.create","action":"App\Http\Controllers\ProgramController@create"},{"host":null,"methods":["POST"],"uri":"programs\/store","name":"programs.store","action":"App\Http\Controllers\ProgramController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"programs\/show","name":"programs.show","action":"App\Http\Controllers\ProgramController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"programs\/{program}\/edit","name":"programs.edit","action":"App\Http\Controllers\ProgramController@edit"},{"host":null,"methods":["POST"],"uri":"programs\/listByCat","name":"programs.cat.list","action":"App\Http\Controllers\ProgramController@listByCat"},{"host":null,"methods":["PUT"],"uri":"programs\/{program}","name":"programs.update","action":"App\Http\Controllers\ProgramController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"cours","name":"cours.index","action":"App\Http\Controllers\CoursController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"cours\/create","name":"cours.create","action":"App\Http\Controllers\CoursController@create"},{"host":null,"methods":["POST"],"uri":"cours\/store","name":"cours.store","action":"App\Http\Controllers\CoursController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"cours\/{cour}\/edit","name":"cours.edit","action":"App\Http\Controllers\CoursController@edit"},{"host":null,"methods":["POST"],"uri":"cours\/listByProgram","name":"cours.program.list","action":"App\Http\Controllers\CoursController@listByProgram"},{"host":null,"methods":["PUT"],"uri":"cours\/{cour}","name":"cours.update","action":"App\Http\Controllers\CoursController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"teacher","name":"teachers.index","action":"App\Http\Controllers\TeachersController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"teacher\/create","name":"teachers.create","action":"App\Http\Controllers\TeachersController@create"},{"host":null,"methods":["POST"],"uri":"teacher\/store","name":"teachers.store","action":"App\Http\Controllers\TeachersController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"teacher\/{teacher}\/edit","name":"teachers.edit","action":"App\Http\Controllers\TeachersController@edit"},{"host":null,"methods":["PUT"],"uri":"teacher\/{teacher}","name":"teachers.update","action":"App\Http\Controllers\TeachersController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"excuses","name":"excuses.index","action":"App\Http\Controllers\ExcuseController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"excuses\/{attendance}","name":"excuses.create","action":"App\Http\Controllers\ExcuseController@create"},{"host":null,"methods":["POST"],"uri":"excuses\/store","name":"excuses.store","action":"App\Http\Controllers\ExcuseController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"excuse\/{excuse}","name":"excuses.show","action":"App\Http\Controllers\ExcuseController@show"},{"host":null,"methods":["POST"],"uri":"groups\/list\/byCourProgram","name":"groups.courprogram.list","action":"App\Http\Controllers\GroupController@groupsByCourProgram"},{"host":null,"methods":["GET","HEAD"],"uri":"groups\/{group}","name":"groups.show","action":"App\Http\Controllers\GroupController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"groups","name":"groups.index","action":"App\Http\Controllers\GroupController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"groups\/create","name":"groups.create","action":"App\Http\Controllers\GroupController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"groups\/{group}\/edit","name":"groups.edit","action":"App\Http\Controllers\GroupController@edit"},{"host":null,"methods":["POST"],"uri":"groups\/store","name":null,"action":"App\Http\Controllers\GroupController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"levels","name":"levels.index","action":"App\Http\Controllers\LevelController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"levels\/create","name":"levels.create","action":"App\Http\Controllers\LevelController@create"},{"host":null,"methods":["POST"],"uri":"levels\/store","name":"levels.store","action":"App\Http\Controllers\LevelController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"levels\/{level}\/edit","name":"levels.edit","action":"App\Http\Controllers\LevelController@edit"},{"host":null,"methods":["PUT"],"uri":"levels\/{level}","name":"levels.update","action":"App\Http\Controllers\LevelController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"classe","name":"classe.index","action":"App\Http\Controllers\ClassController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"classe\/create","name":"classe.create","action":"App\Http\Controllers\ClassController@create"},{"host":null,"methods":["POST"],"uri":"classe\/store","name":"classe.store","action":"App\Http\Controllers\ClassController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"classe\/{classe}\/edit","name":"classe.edit","action":"App\Http\Controllers\ClassController@edit"},{"host":null,"methods":["PUT"],"uri":"classe\/{classe}","name":"classe.update","action":"App\Http\Controllers\ClassController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"reports","name":"reports.index","action":"App\Http\Controllers\ReportController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"reports\/create","name":"reports.create","action":"App\Http\Controllers\ReportController@create"},{"host":null,"methods":["POST"],"uri":"reports\/store","name":"reports.store","action":"App\Http\Controllers\ReportController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"reports\/{report}\/edit","name":"reports.edit","action":"App\Http\Controllers\ReportController@edit"},{"host":null,"methods":["PUT"],"uri":"reports\/{report}","name":"reports.update","action":"App\Http\Controllers\ReportController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"schedules","name":"schedules.index","action":"App\Http\Controllers\ScheduleController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"schedules\/create","name":"schedules.create","action":"App\Http\Controllers\ScheduleController@create"},{"host":null,"methods":["POST"],"uri":"schedules\/store","name":"schedules.store","action":"App\Http\Controllers\ScheduleController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"schedules\/{schedule}\/edit","name":"schedules.edit","action":"App\Http\Controllers\ScheduleController@edit"},{"host":null,"methods":["PUT"],"uri":"schedules\/{schedule}","name":"schedules.update","action":"App\Http\Controllers\ScheduleController@update"},{"host":null,"methods":["POST"],"uri":"calendarweek\/list\/byDate","name":"calendarweek.list","action":"App\Http\Controllers\CalendarWeekController@weekByDate"},{"host":null,"methods":["GET","HEAD"],"uri":"calendarweek","name":"calendarweek.index","action":"App\Http\Controllers\CalendarWeekController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"calendarweek\/create","name":"calendarweek.create","action":"App\Http\Controllers\CalendarWeekController@create"},{"host":null,"methods":["POST"],"uri":"calendarweek\/store","name":"calendarweek.store","action":"App\Http\Controllers\CalendarWeekController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"calendarweek\/{calendarweek}\/edit","name":"calendarweek.edit","action":"App\Http\Controllers\CalendarWeekController@edit"},{"host":null,"methods":["PUT"],"uri":"calendarweek\/{calendarweek}","name":"calendarweek.update","action":"App\Http\Controllers\CalendarWeekController@update"},{"host":null,"methods":["POST"],"uri":"catweekdays\/getByDate","name":"catweekdays.getByDate","action":"App\Http\Controllers\CatWeekdayController@catWeekdaysByDate"},{"host":null,"methods":["GET","HEAD"],"uri":"catweekdays","name":"catweekdays.index","action":"App\Http\Controllers\CatWeekdayController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"catweekdays\/{cat}\/edit","name":"catweekdays.edit","action":"App\Http\Controllers\CatWeekdayController@edit"},{"host":null,"methods":["PUT"],"uri":"catweekdays\/{cat}","name":"catweekdays.update","action":"App\Http\Controllers\CatweekdayController@update"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

