let CreateAttendance = function () {

	let initListeners = function(){
		$('#datetime').change(selectFecha);
		//$('#week').change(selectWeek);
		$('#catname').change(selectCat);
		$('#progname').change(selectProgram);
		$('#level').change(selectLevel);
		$('#group').change(selectGroup);
		$('#datetime').change();
		// HJR
		$('#curname').change(selectCourse);
	}

	let selectFecha = function(){
		let el = $(this);
		let date = el.val();
		let token = $('input[name=_token]').val()
		if(date === "")
			return;
		$.post(laroute.route('calendarweek.list'), {date, _token : token}, function(resp, error){
				// console.log(resp)
				$('#week').val(resp.week)
		});

		$.post(laroute.route('catweekdays.getByDate'), {date, _token : token}, function(resp, error){
			$('#catname').html('')
			$('#catname').append('<option value="-1">Seleccione</option>');
			resp.forEach(function(row){
				$('#catname').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
		});
/*
		let selectTime =  function(){
		let el = $(this);
		let init_time= el.val();
		let end_time= el.val();
		let token = $('input[name=_token]').val()

		$.post(laroute.route('schedules.list'), {init_time, _token : token}, function(resp, error){
			console.log(resp)
				$('#').html('')
			resp.forEach(function(row){
				$('#').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
		});
*/	}


	let selectCat = function(){
		let el = $(this);
		let cat_id = el.val();
		let program_id = el.val();
		let token = $('input[name=_token]').val()


		/*$.post(laroute.route('classrooms.list'), {cat_id, _token : token}, function(resp, error){
			console.log(resp)
				$('#classroom_id').html('')
			resp.forEach(function(row){
				$('#classroom_id').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
		});*/

		$.post(laroute.route('programs.cat.list'), {cat_id, _token : token}, function(resp, error){
			//console.log(resp)
				$('#progname').html('')
				//$('#progname').append('<option value="0">Seleccione</option>');
			resp.forEach(function(row){
				$('#progname').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
			// HJR
			$('#progname').change();
		});



	/*$.post(laroute.route('levels.cours.list'), {cour_id, _token : token}, function(resp, error){
		console.log(resp)
			$('#curname').html('')
		resp.forEach(function(row){
			$('#curname').append('<option value="'+row.id+'">'+row.name+'</option>');
		});
	});*/


	}

	let selectProgram =  function(){
		let el = $(this);
		let program_id = el.val();
		let token = $('input[name=_token]').val()

		$.post(laroute.route('cours.program.list'), {program_id, _token : token}, function(resp, error){
			//console.log(resp)
				$('#curname').html('')
			resp.forEach(function(row){
				$('#curname').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
			$('#curname').change();
		});
	}

	let selectLevel =  function(){
		let el = $(this);
		let level_id = el.val();
		let program_id =  $('#progname').val();
		let cour_id =  $('#curname').val();
		let token = $('input[name=_token]').val()
		let data = {
			program_id,
			level_id,
			cour_id,
			_token : token,
			}
			//console.log(data);
		$.post(laroute.route('groups.courprogram.list'), data, function(resp, error){
			//console.log(resp)
				$('#group').html('');
				//$('#group').append('<option value="0">Seleccione</option>');
			resp.forEach(function(row){
				$('#group').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
			$('#group').change();
		});
	}

	let selectGroup = function(){
		let el = $(this);
		let group_id = el.val();
		let token = $('input[name=_token]').val();
		if(group_id=='' || group_id==null || group_id==undefined){
			alert('No existe un grupo para este curso.');
			return
		}

		$.get(laroute.route('groups.show', {group:group_id}),function(group, error){
			if(group.length <= 0)
				return;
			// console.log(group);
			let teacher_name = group.teacher.name;
			let classroom = group.classroom.name;
			$("#docname").val(teacher_name);
			$("#classroom_id").val(classroom);
		});
	}

	// HJR
	let selectCourse = function(){
		let el = $(this);
		let cour_id = el.val();
		let program_id =  $('#progname').val();
		let token = $('input[name=_token]').val()
		let data = {
			program_id,
			cour_id,
			_token : token
			}
		$.post('/cours-levels/listLevels', data, function(resp, error){
				$('#level').html('')
			resp.forEach(function(row){
				$('#level').append('<option value="'+row.id+'">'+row.name+'</option>');
			});
			$('#level').change();
		});
	}

	return {
		init: function() {
			initListeners();
		}
	}
}();

$(CreateAttendance.init)