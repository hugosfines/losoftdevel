@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<div class="card">
                {{ csrf_field() }}
                <div class="card-header">
                    Crear asistencia
                </div>

                <div class="card-body">             
                	<div class="row">
                    {{ Form::open(['route' => 'excuses.store', 'method' => 'POST', 'files' => true]) }}
                    <div class="col-md-6">
                        {{Form::label('excuse_file')}}
                    	{{Form::file('excuse_file')}}
                    	{{Form::hidden('attendance_id', $attendance)}}
                    </div>	

                    <div class="form-group">
					    <div class="row">
					        <div class="col-md-2 offset-10">

					            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}  
					            
					        </div>
					    </div>
					</div>
                    {{ Form::close() }}
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection