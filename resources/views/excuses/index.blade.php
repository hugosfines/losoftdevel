@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Inasistencia  
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>CAT-Salon</th>
                                <th>Profesional</th>
                                <th>Estudiantes</th>
                                <th>Fecha</th>
                                <th>Asistio</th>
                                <th>Excusa?</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Attendances as $Attendance)
                            <tr>
                                <td>{{ $Attendance->classroom->cat->name }}-{{ $Attendance->classroom->name }} </td>
                                <td>{{ $Attendance->professional->name }}</td>
                                <td>{{ $Attendance->students }}</td>
                                <td>{{ $Attendance->created_at }}</td>
                                <td>{{ $Attendance->attended ? 'Si' : 'No'  }}</td>
                                @can('excuses.upload')
                                <td>
                                   @if(isset($Attendance->excuse->id))
                                    Si
                                   @else
                                   {{ Form::open( array('route' => 'excuses.creae', 'id' => 'excuse-form', 'enctype' => 'multipart/form-data')) }}

                                        {{ Form::hidden('attendance_id', $Attendance->id) }}
                                        {{ Form::file('excuse', array('onChange' => 'event.preventDefault(); document.getElementById("excuse-form").submit();')) }}
                                                                       
                                   {{ Form::close() }}
                                   @endif
                                </td>
                                @endcan
                                                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $Attendances->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
