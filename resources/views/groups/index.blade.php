@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Grupos
                    @can('groups.create')
                    <a href="{{ route('groups.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Grupo</th>
                                <th>Programa Y Curso</th>
                                <th>Docente</th>
                                <th>Salon</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($groups as $group)
                            <tr>
                                <td>{{ $group->id }}</td>
                                <td>{{ $group->name }}</td>
                                <td>{{ $group->cour_program->name }}</td>
                                <td>{{ $group->teacher->name }}</td>
                                 <td>{{ $group->classroom->name }}</td>

                                @can('groups.edit')
                                    <td width="10px">
                                        <a href="{{ route('groups.edit', $group->id) }}"
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $groups->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection