<div class="form-group">
	<div class="row">
		<div class="col-md-12">
			{{ Form::label('name', 'Nombre Grupo')}}
			{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
		</div>
	</div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('cour_program_id', 'Programa y Curso')}}
           {{ Form::select('cour_program_id', $courprogram, null,
        ['class'=>'form-control', 'id' => 'name']) }}
       </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('teacher_id', 'Docente')}}
            {{ Form::select('teacher_id',$teacher, null, ['class' => 'form-control', 'id' => 'teacher_id']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('classroom_id', 'Salon')}}
            {{ Form::select('classroom_id', $classroom, null, ['class' => 'form-control', 'id' => 'classroom_id']) }}
        </div>
    </div>
</div>
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
		</div>
	</div>
</div>