@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Modificar Grupo
                </div>
                <div class="card-body">
                    {!! Form::model($group, ['route' => ['groups.update', $group->id], 'method' => 'PUT']) !!}

                        @include('groups.partials.form')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection