<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('group_id', 'Grupo')}}
            {{ Form::select('group_id', $group, null,  ['class' => 'form-control', 'id' => 'group_id']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('nombre', 'Nombre')}}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('week', 'Semana')}}
            {{ Form::select('week', [1 => '1', 2 => '2'], null, ['class' => 'form-control', 'id' => 'week']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('tutorial', 'Tutoria')}}
            {{ Form::select('tutorial', [1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5', 6=>'6', 7=>'7', 8=>'8'], null, ['class' => 'form-control', 'id' => 'tutorial']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
       <div class="col-md-3">
            {!! Form::label('datetime', 'Fecha Y Hora')!!}
            {!! Form::datetime('datetime', "", ['class' => 'form-control', 'id' => 'datetime']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</div>
