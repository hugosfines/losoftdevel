@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    CLASES
                    @can('classe.create')
                    <a href="{{ route('classe.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Grupo</th>
                                <th>Clase</th>
                                <th>Semana</th>
                                <th>Tutorial</th>
                                <th>Fecha Y Hora</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classes as $class)
                            <tr>
                                <td>{{ $class->id }}</td>
                                <td>{{ $class->group->name }}</td>
                                <td>{{ $class->name }}</td>
                                <td>{{ $class->week }}</td>
                                <td>{{ $class->tutorial }}</td>
                                <td>{{ $class->datetime }}</td>

                                @can('classe.edit')
                                    <td width="10px">
                                        <a href="{{ route('classe.edit', $class->id) }}"
                                        class="btn btn-primary">
                                            Editar
                                        </a>
                                    </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $classes->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection