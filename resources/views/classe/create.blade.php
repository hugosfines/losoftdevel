@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Crear Horario
                </div>

                <div class="card-body">
                    {{ Form::open(['route' => 'classe.store']) }}

                       @include('classe.partials.form')

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection