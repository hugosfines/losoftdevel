<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			{{ Form::label('name', 'Nombre')}}
			{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
		</div>
        <div class="col-md-6">
            {{ Form::label('email', 'Correo Electronico')}}
            {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) }}
        </div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
				{{ Form::label('password', 'Contraseña')}}
				{{ Form::password('password', ['class' => 'form-control', 'id' => 'password']) }}
			</div>
		<div class="col-md-6">
			{{ Form::label('password-confirm', 'Confirmar contraseña') }}
			{{ Form::password('password_confirm', ['class' => 'form-control', 'id' => 'password_confirm']) }}
		</div>
	</div>
</div>
@unless(!Auth::check())
<hr>
<h3>Lista de roles</h3>
<div class="form-group">
	<ul class="list-unstyled">
		@foreach($roles as $role)
	    <li>
	        <label>
	        {{ Form::checkbox('roles[]', $role->id, null) }}
	        {{ $role->name }}	<em>({{ $role->description }})</em>
	        </label>
	    </li>
	    @endforeach
    </ul>
</div>
@endunless
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
		</div>
	</div>
</div>