@extends('layouts.app')

@section('content')
<div class="container">
    @can('attendances.index')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <a href="{{ route('attendances.index') }}">
                <div class="jumbotron jumbotron-fluid bg-success">
                    <div class="container">
                        <h1 class="display-4">Registro de asistencia</h1>
                    </div>
                </div>
            </a>
        </div>
    </div>
    @endcan

 @can('roles.index')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <a href="{{ route('roles.index') }}">
                    <div class="jumbotron jumbotron-fluid bg-warning">
                        <div class="container">
                         <h1 class="display-4">Panel de Administraciòn</h1>
                        </div>
                    </div>
                </a>                    
            </div>
        </div>
    </div>
</div>
 @endcan
@endsection
