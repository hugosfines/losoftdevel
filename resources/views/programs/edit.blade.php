@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Modificar CAT
                </div>

                <div class="card-body">                                       
                    {!! Form::model($program, ['route' => ['programs.update', $program->id], 'method' => 'PUT']) !!}

                        @include('programs.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection