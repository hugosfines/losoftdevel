@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Programa
                    @can('programs.create')
                    <a href="{{ route('programs.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Programa</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($programs as $program)
                            <tr>
                                <td>{{ $program->id }}</td>
                                <td>{{ $program->name }}</td>
                               
                                @can('programs.edit')
                                    <td width="10px">
                                        <a href="{{ route('programs.edit', $program->id) }}" 
                                        class="btn btn-primary">
                                            Editar
                                        </a>
                                    </td>
                                @endcan                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $programs->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection