<div class="form-group">
    <div class="row">

      {!! Form::open(['route' => 'attendances.store', 'method'=>'POST']) !!}

       <div class="col-md-3">
            {!! Form::label('datetime', 'Fecha')!!}
            {!! Form::date('datetime', date('Y-m-d'), ['class' => 'form-control', 'id' => 'datetime']) !!}
        </div>
         <div class="col-md-3">
            {!! Form::label('week', 'Semana')!!}
            {!! Form::text('week', 'Seleccione fecha',  ['class' => 'form-control', 'id' => 'week', 'readonly']) !!}
        </div>

        <div class="col-md-6">
            {!! Form::label('catname', 'CAT')!!}
            {!! Form::select('catname', [], null,  ['class' => 'form-control', 'id' => 'catname']) !!}
        </div>

        <div class="col-md-6">
            {!! Form::label('progname', 'Programa')!!}
            {!! Form::select('progname', [], null,  ['class' => 'form-control', 'id' => 'progname', 'placeholder' => 'Seleccione el programa']) !!}
        </div>

        <div class="col-md-6">
            {!! Form::label('curname', 'Curso')!!}
            {!! Form::select('curname', [], null,  ['class' => 'form-control', 'id' => 'curname', 'placeholder' => 'Seleccione el curso']) !!}
        </div>

        <div class="col-md-6">
            {!! Form::label('level', 'Nivel')!!}
            {{ Form::select('level', [], null,  ['class' => 'form-control', 'id' => 'level', 'placeholder' => 'Seleccione nivel']) }}
            {{-- 
                {!! Form::select('level', $levels, '0',  ['class' => 'form-control', 'id' => 'level']) !!}
            --}}
        </div>

         <div class="col-md-6">
            {!! Form::label('group', 'Grupo')!!}
            {!! Form::select('group', [], null,  ['class' => 'form-control', 'id' => 'group']) !!}
        </div>

        <div class="col-md-5">
            {!! Form::label('docname', 'Docente')!!}
            {!! Form::text('docname', '', ['class' => 'form-control', 'id' => 'docname', 'readonly']) !!}
        </div>
         <div class="col-md-3">
            {!! Form::label('classroom_id', 'Salon')!!}
            {!! Form::text('classroom_id', '', ['class' => 'form-control', 'id' => 'classroom_id', 'readonly']) !!}
        </div>

        <div class="col-md-2">
            {!! Form::label('students', '# Estudiantes')!!}
            {!! Form::number('students', "", ['class' => 'form-control', 'id' => 'students', 'value' => old('# Estudiantes'), 'min' => 0] ) !!}

        </div>
        <div class="col-md-2">
            {!! Form::label('tutorial', 'Tutoria')!!}
            {!! Form::select('tutorial', ['1'=>'1', '2'=>'2', '3'=>'3','4'=>'4', '5'=>'5', '6'=>'6', '7'=>7, '8'=>8],null, ['class' => 'form-control', 'id' => 'tutorial']) !!}
        </div>

        <div class="col-md-4">
            {!! Form::label('attended', 'Asistio')!!}
            {!! Form::select('attended', [1=>'Si', 0=>'No'], null,['class' => 'form-control', 'id' => 'attended']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::label('user_id', 'Funcionario Responsable')!!}
            {!! Form::text('user_id', auth()->user()->name,  ['readonly', 'class' => 'form-control', 'id' => 'user_id']) !!}
        </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-2 offset-10">

            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

        </div>
    </div>
</div>

