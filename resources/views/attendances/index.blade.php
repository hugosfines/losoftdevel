@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Asistencia
                    @can('attendances.create')
                    <a href="{{ route('attendances.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <div class="table-responsive"> 
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10px">CAT</th>
                                <th>Salon</td>
                                <th>Funcionario Responsable</th>
                                <th>Dia-Hora</th>
                                <th>Semana</th>
                                <th>Programa</th>
                                <th>Nivel</th>
                                <th>Grupo</th>
                                <th>Asignatura</th>
                                <th>Docente</th>
                                <th>Estudiantes</th>
                                <th>Tutoria</th>
                                <th>Fecha</th>
                                <th>Asistio</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Attendances as $Attendance)
                            
                            <tr>
                                <td>{{ $Attendance->lesson->group->classroom->cat->name }}</td>
                                <td>{{ $Attendance->lesson->group->classroom->name }} </td>
                                <td>{{ $Attendance->user->name}}</td>
                                <td>{{ $Attendance->lesson->day_hour}}</td>
                                <td>{{ $Attendance->lesson->week}}</td>
                                <td>{{ $Attendance->lesson->group->cour_program->program->name}}</td>
                                <td>{{ $Attendance->lesson->group->cour_program->level->name}}</td>
                                <td>{{ $Attendance->lesson->group->name}}</td>
                                <td>{{ $Attendance->lesson->group->cour_program->cour->name}}</td>
                                <td>{{ $Attendance->lesson->group->teacher->name }}</td>
                                <td>{{ $Attendance->students }}</td>
                                <td>{{ $Attendance->lesson->tutorial }}</td>
                                <td>{{ $Attendance->lesson->datetime }}</td>
                                <td>{{ $Attendance->was_attended }}</td>
                                
                                    <td width="10px">
                                @can('attendance.edit')
                                        <a href="{{ route('attendances.edit', $Attendance->id) }}" 
                                        class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                @endcan
                                @if(!$Attendance->attended and $Attendance->excuses->count() <= 0)      
                                    <a href="{{ route('excuses.create', $Attendance->id) }}" 
                                        class="btn btn-warning">
                                            <i class="fa fa-upload"></i>
                                    </a>              
                                @endif
                                @if(!$Attendance->attended and $Attendance->excuses->count() > 0)      
                                    <a href="{{ route('excuses.show', $Attendance->excuses->first()->id) }}" 
                                        class="btn btn-success">
                                            <i class="fa fa-download"></i>
                                    </a>              
                                @endif         
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="{{ route('attendances.download') }}">Descargar PDF</a>
                    </div>
                    {{ $Attendances->render() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection