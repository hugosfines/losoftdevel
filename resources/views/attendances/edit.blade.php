@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Modificar asistencia
                </div>

                <div class="card-body">                                       
                    {!! Form::model($attendance, ['route' => ['attendances.update', $attendance->id], 'method' => 'PUT']) !!}

                        @include('attendances.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection