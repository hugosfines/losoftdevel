@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                {{ csrf_field() }}
                <div class="card-header" style="text-align: center; font-size: 20px">
                  ASISTENCIA LABOR DOCENTE
                  </div>
                <div class="card-header">
                    Crear asistencia
                </div>

            @if($errors->any())
            <div class="alert alert-danger">
              
                   <ul>
                    @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>              
                       @endforeach                   
                   </ul>
            </div>
                        
            @endif  
            @if(session('fail'))
            <div class="alert alert-danger">
              
                   {{session('fail')}}
            </div>
                        
            @endif 
                <div class="card-body">                    
                    {{ Form::open(['route' => 'attendances.store']) }}

                       @include('attendances.partials.form')
                        
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{url('js/attendance/create.js')}}"></script>
@endsection