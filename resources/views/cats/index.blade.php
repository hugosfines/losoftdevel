@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    CAT
                    @can('cats.create')
                    <a href="{{ route('cats.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>CAT</th>
                                <th>ACTIVO?</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cats as $cat)
                            <tr>
                                <td>{{ $cat->id }}</td>
                                <td>{{ $cat->name }}</td>
                                <td>{{ $cat->activo }}</td>

                                @can('cats.edit')
                                    <td width="10px">
                                        <a href="{{ route('cats.edit', $cat->id) }}"
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan
                                @can('cats.destroy')
                                <td width="10px">
                                    {!! Form::open(['route' => ['cats.destroy', $cat->id],
                                    'method' => 'DELETE']) !!}
                                        <button class="btn btn-sm btn-danger">
                                            Eliminar
                                        </button>
                                    {!! Form::close() !!}
                                    </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $cats->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection