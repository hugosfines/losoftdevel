@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Editar horario - grupo 
                </div>

                <div class="card-body">                     
                    {!! Form::model($schedule, ['route' => ['schedules.update', $schedule->id], 'method' => 'PUT']) !!}

                        @include('schedules.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection