@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Horarios De Clase
                    @can('schedules.create')
                    <a href="{{ route('schedules.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Dia-Semana</th>
                                <th>Inicio-Tiempo</th>
                                <th>Final-Tiempo</th>
                                <th>Fecha</th>
                                <th>Semana</th>                  
                                <th>Grupo</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($schedules as $schedule)
                            <tr>
                                <td>{{ $schedule->id }}</td>
                                <td>{{ $schedule->day_week}}</td>
                                <td>{{ $schedule->init_time}}</td>
                                <td>{{ $schedule->end_time}}</td>
                                <td>{{ $schedule->date}}</td>
                                <td>{{ $schedule->week}}</td>
                                <td>{{ $schedule->group->name }}</td>
                                
                                @can('schedules.edit')
                                    <td width="10px">
                                        <a href="{{ route('schedules.edit', $schedule->id) }}" 
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan                                  
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $schedules->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


