<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('group_id', 'Grupo')}}
            {{ Form::select('group_id', $group, null,  ['class' => 'form-control', 'id' => 'group_id']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">

        <div class="col-md-12">
            {{ Form::label('day_week', 'Dia-Semana')}}
            {{ Form::text('day_week', null, ['class' => 'form-control', 'id' => 'day_week']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('init_time', 'Inicio Tiempo')}}
            {{ Form::time('init_time', null, ['class' => 'form-control', 'id' => 'init_time']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('end_time', 'Final Tiempo')}}
            {{ Form::time('end_time', null, ['class' => 'form-control', 'id' => 'end_time']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('week', 'Semana')}}
            {{ Form::text('week', null, ['class' => 'form-control', 'id' => 'week']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</div>
