@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Curso
                    @can('cours.create')
                    <a href="{{ route('cours.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Curso</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cours as $cour)
                            <tr>
                                <td>{{ $cour->id }}</td>
                                <td>{{ $cour->name }}</td>

                                @can('cours.edit')
                                    <td width="10px">
                                        <a href="{{ route('cours.edit', $cour->id) }}" 
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan
                                @can('cours.destroy')
                                <td width="10px">
                                    {!! Form::open(['route' => ['cours.destroy', $cour->id], 
                                    'method' => 'DELETE']) !!}
                                        <button class="btn btn-sm btn-danger">
                                            Eliminar
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                                @endcan
                            </tr>                                  
                            @endforeach
                        </tbody>
                    </table>
                    {{ $cours->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection