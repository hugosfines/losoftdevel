@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    CLASES
                    @can('catweekdays.create')
                    <a href="" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th rowspan="2">CAT</th>
                                <th colspan="7">Dia</th>
                                <th rowspan="2">Acciones</th>
                            </tr>
                            <tr>
                                <th>Lunes</th>
                                <th>Martes</th>
                                <th>Miercoles</th>
                                <th>Jueves</th>
                                <th>Viernes</th>
                                <th>Sabado</th>
                                <th>Domingo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cats as $cat)
                            <tr>
                                <td>{{$cat->name}}</td>
                                @for($i = 1; $i <= 7; $i++)
                                    <td style="text-align: center">
                                        @if($cat->weekdayStatus($i))
                                            <i class="fa fa-check text-success"></i>
                                        @else
                                            <i class="fa fa-window-close text-danger"></i>
                                        @endif
                                    </td>
                                @endfor
                                <td style="text-align: center">
                                    <a href="{{route('catweekdays.edit',$cat->id)}}">
                                        <i class="fa fa-edit text-primary"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection