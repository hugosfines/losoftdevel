<div class="form-group">
    <div class="row">
        <div class="col-md-3">
            {{ Form::label('lunes', 'Lunes')}}
            {{ Form::select('lunes-1', $options, $cat->weekdayStatus(1),  ['class' => 'form-control', 'id' => 'lunes']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('martes', 'Martes')}}
            {{ Form::select('martes-2', $options, $cat->weekdayStatus(2),  ['class' => 'form-control', 'id' => 'martes']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('miercoles', 'Miercoles')}}
            {{ Form::select('miercoles-3', $options, $cat->weekdayStatus(3),  ['class' => 'form-control', 'id' => 'miercoles']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('jueves', 'Jueves')}}
            {{ Form::select('jueves-4', $options, $cat->weekdayStatus(4),  ['class' => 'form-control', 'id' => 'jueves']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-3">
            {{ Form::label('viernes', 'Viernes')}}
            {{ Form::select('viernes-5', $options, $cat->weekdayStatus(5),  ['class' => 'form-control', 'id' => 'viernes']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('sabado', 'Sábado')}}
            {{ Form::select('sabado-6', $options, $cat->weekdayStatus(6),  ['class' => 'form-control', 'id' => 'sabado']) }}
        </div>
        <div class="col-md-3">
            {{ Form::label('domingo', 'Domingo')}}
            {{ Form::select('domingo-7', $options, $cat->weekdayStatus(7),  ['class' => 'form-control', 'id' => 'domingo']) }}
        </div>
        <div class="col-md-3">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg', 'style' => 'margin-top:23px']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        
    </div>
</div>
