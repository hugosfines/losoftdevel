@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Docentes
                    @can('teachers.create')
                    <a href="{{ route('teachers.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Docente</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teachers as $teacher)
                            <tr>
                                <td>{{ $teacher->id }}</td>
                                <td>{{ $teacher->name }}</td>

                                @can('teachers.edit')
                                    <td width="10px">
                                        <a href="{{ route('teachers.edit', $teacher->id) }}" 
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan                                  
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $teachers->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection