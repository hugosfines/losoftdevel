<div class="form-group">
	<div class="row">
		<div class="col-md-12">
			{{ Form::label('name', 'Nombre Docente')}}
			{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
		</div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
		</div>
	</div>
</div>