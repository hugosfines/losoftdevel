<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('init_date', 'Inicio Clase')}}
            {!! Form::date('init_date', date('Y-m-d'), ['class' => 'form-control', 'id' => 'init_date']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('end_date', 'Fin Clase')}}
            {!! Form::date('end_date', date('Y-m-d'), ['class' => 'form-control', 'id' => 'end_date']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('nweek', '#Semanas')}}
            {{ Form::text('nweek', null, ['class' => 'form-control', 'id' => 'nweek']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('week', 'Semana')}}
            {{ Form::text('week', null, ['class' => 'form-control', 'id' => 'week']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</div>
