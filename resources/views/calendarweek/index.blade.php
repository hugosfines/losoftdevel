@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    CLASES
                    @can('calendarweek.create')
                    <a href="{{ route('calendarweek.create') }}" class="btn btn-success float-right">
                        Crear Calendario - Semana
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Inicio Clase</th>
                                <th>Fin Clase</th>
                                <th>#Semanas</th>
                                <th>Semana</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($calendarweeks as $calendarweek)
                            <tr>
                                <td>{{ $calendarweek->id }}</td>
                                <td>{{ $calendarweek->init_date }}</td>
                                <td>{{ $calendarweek->end_date }}</td>
                                <td>{{ $calendarweek->nweek }}</td>
                                <td>{{ $calendarweek->week }}</td>
                                @can('calendarweek.edit')
                                    <td width="10px">
                                        <a href="{{ route('calendarweek.edit', $calendarweek->id) }}" calendarweek="btn btn-primary">
                                            Editar
                                        </a>
                                    </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $calendarweeks->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection