@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    HORARIOS
                    @can('classrooms.create')
                    <a href="{{ route('classrooms.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>CAT</th>
                                <th>Salon</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classrooms as $classroom)
                            <tr>
                                <td>{{ $classroom->id }}</td>
                                <td>{{ $classroom->cat->name }}</td>
                                <td>{{ $classroom->name }}</td>
                                
                                @can('classrooms.edit')
                                    <td width="10px">
                                        <a href="{{ route('classrooms.edit', $classroom->id) }}" 
                                        class="btn btn-primary">
                                            Editar
                                        </a>
                                    </td>
                                @endcan                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $classrooms->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection