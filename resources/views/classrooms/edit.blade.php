@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Editar horario
                </div>

                <div class="card-body">                     
                    {!! Form::model($classroom, ['route' => ['classrooms.update', $classroom->id], 'method' => 'PUT']) !!}

                        @include('classrooms.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection