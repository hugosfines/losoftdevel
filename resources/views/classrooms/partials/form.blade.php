<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('cat_id', 'CAT')}}
            {{ Form::select('cat_id', $cat, null,  ['class' => 'form-control', 'id' => 'cat_id']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            {{ Form::label('nombre', 'Nombre')}}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</div>
