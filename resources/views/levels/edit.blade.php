@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Modificar Level
                </div>

                <div class="card-body">                                       
                    {!! Form::model($level, ['route' => ['levels.update', $level->id], 'method' => 'PUT']) !!}

                        @include('levels.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection