@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Cursos
                    @can('levels.create')
                    <a href="{{ route('levels.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nivel</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($levels as $level)
                            <tr>
                                <td>{{ $level->id }}</td>
                                <td>{{ $level->name }}</td>

                                @can('levels.edit')
                                    <td width="10px">
                                        <a href="{{ route('levels.edit', $level->id) }}" 
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan                                  
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $levels->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection