@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Modificar Reporte
                </div>

                <div class="card-body">                                       
                    {!! Form::model($report, ['route' => ['reports.update', $report->id], 'method' => 'PUT']) !!}

                        @include('reports.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection