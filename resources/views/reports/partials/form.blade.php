<div class="form-group">
	<div class="row">
		<div class="col-md-12">
			{{ Form::label('observations', 'Observaciones')}}
			{{ Form::text('observations', null, ['class' => 'form-control', 'id' => 'observations']) }}
		</div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<div class="col-md-12">
			{{ Form::label('Indication', 'Indicaciones del Departamento')}}
			{{ Form::text('Indication', null, ['class' => 'form-control', 'id' => 'Indication']) }}
		</div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
		</div>
	</div>
</div>