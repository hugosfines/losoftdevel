@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Reportes
                    @can('reports.create')
                    <a href="{{ route('reports.create') }}" class="btn btn-success float-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Observaciones</th>
                                <th>Indicaciones del Departamento</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($reports as $report)
                            <tr>
                                <td>{{ $report->id }}</td>
                                <td>{{ $report->observations}}</td>
                                <td>{{ $report->Indication}}</td>
                                
                                @can('reports.edit')
                                    <td width="10px">
                                        <a href="{{ route('reports.edit', $report->id) }}" 
                                        class="btn btn-primary">
                                            Modificar
                                        </a>
                                    </td>
                                @endcan                                  
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $reports->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection