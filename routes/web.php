<?php

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Email Verification Routes...
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');





Route::middleware(['auth'])->group(function () {


    Route::get('/', 'HomeController@index');

    Route::get('/home', 'HomeController@index')->name('home');

    //USERS
    Route::get('users', 'UserController@index')
        ->name('users.index')
        ->middleware('can:users.index');

    Route::get('users/create', 'UserController@create')
        ->name('users.create')
        ->middleware('can:users.create');

    Route::post('users/store', 'UserController@store')
        ->name('users.store')
        ->middleware('can:users.create');

    Route::delete('users/{user}', 'UserController@destroy')
        ->name('users.destroy')
        ->middleware('can:users.destroy');

    Route::get('users/{user}/edit','UserController@edit')
        ->name('users.edit')
        ->middleware('can:users.edit');

    Route::put('users/{user}', 'UserController@update')
        ->name('users.update')
        ->middleware('can:users.edit');

    //ROLES
    Route::get('roles', 'RoleController@index')
        ->name('roles.index')
        ->middleware('can:roles.index');

    Route::get('roles/create', 'RoleController@create')
        ->name('roles.create')
        ->middleware('can:roles.create');

    Route::post('roles/store', 'RoleController@store')
        ->name('roles.store')
        ->middleware('can:roles.create');

    Route::get('roles/{role}/edit', 'RoleController@edit')
        ->name('roles.edit')
        ->middleware('can:roles.edit');

    Route::get('roles/{role}', 'RoleController@show')
        ->name('roles.show')
        ->middleware('can:roles.show');

    Route::delete('roles/{role}', 'RoleController@destroy')
        ->name('roles.destroy')
        ->middleware('can:roles.destroy');


    Route::put('roles/{role}/', 'RoleController@update')
        ->name('roles.update')
        ->middleware('can:roles.edit');

    /**RUTA ESPECIAL DE AUDITORIA*/
    Route::get('logs', 'LogController@index')
        ->name('logs.index')
        ->middleware('can:logs.index');

    /**CAT*/
    Route::get('cats', 'CatController@index')
        ->name('cats.index')
        ->middleware('can:cat.index');

    Route::get('cats/create', 'CatController@create')
        ->name('cats.create')
        ->middleware('can:cat.create');

    Route::post('cats/store', 'CatController@store')
        ->name('cats.store')
        ->middleware('can:cat.create');

    Route::get('cats/{cat}/edit', 'CatController@edit')
        ->name('cats.edit')
        ->middleware('can:cat.edit');

    Route::put('cats/{cat}/', 'CatController@update')
        ->name('cats.update')
        ->middleware('can:cat.edit');

    Route::delete('cats/{cat}', 'CatController@destroy')
        ->name('cats.destroy')
        ->middleware('can:cats.destroy');

    //CLASSROOOM

    Route::post('classrooms/list', 'ClassroomController@classroomByCat')
        ->name('classrooms.list')
     ->middleware('can:classrooms.list');

    Route::get('classrooms', 'ClassroomController@index')
        ->name('classrooms.index')
        ->middleware('can:classrooms.index');

    Route::get('classrooms/create', 'ClassroomController@create')
        ->name('classrooms.create')
        ->middleware('can:classrooms.create');

    Route::get('classrooms/show', 'ClassroomController@show')
        ->name('classrooms.show')
        ->middleware('can:classrooms.show');

    Route::post('classrooms/store', 'ClassroomController@store')
        ->name('classrooms.store')
        ->middleware('can:classrooms.create');

    Route::get('classrooms/{classroom}/edit', 'ClassroomController@edit')
        ->name('classrooms.edit')
        ->middleware('can:classrooms.edit');

    Route::put('classrooms/{classroom}/', 'ClassroomController@update')
        ->name('classrooms.update')
        ->middleware('can:classrooms.edit');


    //ATTENDANCE
    Route::get('attendances', 'AttendanceController@index')
        ->name('attendances.index')
        ->middleware('can:attendances.index');

    Route::get('attendances/create', 'AttendanceController@create')
        ->name('attendances.create')
        ->middleware('can:attendances.create');

    Route::post('attendances/store', 'AttendanceController@store')
        ->name('attendances.store')
        ->middleware('can:attendances.create');

    Route::get('attendances/{attendance}/edit', 'AttendanceController@edit')
        ->name('attendances.edit')
        ->middleware('can:attendances.edit');

    Route::get('attendances/download', 'AttendanceController@download')
        ->name('attendances.download')
        ->middleware('can:attendances.download');

    Route::get('attendances/{attendance}', 'AttendanceController@show')
        ->name('attendances.show')
        ->middleware('can:attendances.show');


    Route::put('attendances/{attendance}/', 'AttendanceController@update')
        ->name('attendances.update')
        ->middleware('can:attendances.edit');

    /**Program*/
    Route::get('programs', 'ProgramController@index')
        ->name('programs.index')
        ->middleware('can:program.index');

    Route::get('programs/create', 'ProgramController@create')
        ->name('programs.create')
        ->middleware('can:program.create');

    Route::post('programs/store', 'ProgramController@store')
        ->name('programs.store')
        ->middleware('can:program.create');

    Route::get('programs/show', 'ProgramController@show')
        ->name('programs.show')
        ->middleware('can:programs.show');

    Route::get('programs/{program}/edit', 'ProgramController@edit')
        ->name('programs.edit')
        ->middleware('can:program.edit');

    Route::post('programs/listByCat', 'ProgramController@listByCat')
    ->name('programs.cat.list')
   ->middleware('can:program.cat.list');

    Route::put('programs/{program}/', 'ProgramController@update')
        ->name('programs.update')
        ->middleware('can:program.edit');


        /**Cours*/
    Route::get('cours', 'CoursController@index')
        ->name('cours.index')
        ->middleware('can:cour.index');

    Route::get('cours/create', 'CoursController@create')
        ->name('cours.create')
        ->middleware('can:cour.create');

    Route::post('cours/store', 'CoursController@store')
        ->name('cours.store')
        ->middleware('can:cour.create');

    Route::get('cours/{cour}/edit', 'CoursController@edit')
        ->name('cours.edit')
        ->middleware('can:cour.edit');

    Route::delete('cours/{cour}', 'CoursController@destroy')
        ->name('cours.destroy')
        ->middleware('can:cours.destroy');

    Route::post('cours/listByProgram', 'CoursController@listByProgram')
    ->name('cours.program.list');
//    ->middleware('can:cours.program.list');
    //HJR
    Route::post('/cours-levels/listLevels', 'CoursController@listLevels')
    ->name('cours-levels.list');

    Route::put('cours/{cour}/', 'CoursController@update')
        ->name('cours.update')
        ->middleware('can:cour.edit');

         /**Teachers*/
    Route::get('teacher', 'TeachersController@index')
        ->name('teachers.index')
        ->middleware('can:teachers.index');

    Route::get('teacher/create', 'TeachersController@create')
        ->name('teachers.create')
        ->middleware('can:teachers.create');

    Route::post('teacher/store', 'TeachersController@store')
        ->name('teachers.store')
        ->middleware('can:teachers.create');

    Route::get('teacher/{teacher}/edit', 'TeachersController@edit')
        ->name('teachers.edit')
        ->middleware('can:teacher.edit');

    Route::put('teacher/{teacher}/', 'TeachersController@update')
        ->name('teachers.update')
        ->middleware('can:teachers.edit');


    //EXCUSES
    Route::get('excuses', 'ExcuseController@index')
        ->name('excuses.index')
        ->middleware('can:excuses.index');

    Route::get('excuses/{attendance}', 'ExcuseController@create')
        ->name('excuses.create')
        ->middleware('can:excuses.create');

    Route::post('excuses/store', 'ExcuseController@store')
        ->name('excuses.store')
        ->middleware('can:excuses.create');

    Route::get('excuse/{excuse}', 'ExcuseController@show')
        ->name('excuses.show')
        ->middleware('can:excuse.show');

    //GROUPS
    Route::post('groups/list/byCourProgram', 'GroupController@groupsByCourProgram')
        ->name('groups.courprogram.list')
       ->middleware('can:groups.courprogram.list');
Route::get('groups/create', 'GroupController@create')
        ->name('groups.create')
        ->middleware('can:groups.create');

    Route::get('groups/{group}', 'GroupController@show')
        ->name('groups.show')
        ->middleware('can:group.show');

    Route::get('groups', 'GroupController@index')
        ->name('groups.index')
        ->middleware('can:groups.index');



    Route::get('groups/{group}/edit', 'GroupController@edit')
        ->name('groups.edit')
        ->middleware('can:groups.edit');

    Route::put('groups/{group}/', 'GroupController@update')
        ->name('groups.update')
        ->middleware('can:groups.edit');

  Route::post('groups/store', 'GroupController@store')
        ->name('groups.store')
        ->middleware('can:groups.create');

    //levels
    Route::get('levels', 'LevelController@index')
        ->name('levels.index')
        ->middleware('can:levels.index');

    Route::get('levels/create', 'LevelController@create')
        ->name('levels.create')
        ->middleware('can:levels.create');

    Route::post('levels/store', 'LevelController@store')
        ->name('levels.store')
        ->middleware('can:level.create');

    Route::get('levels/{level}/edit', 'LevelController@edit')
        ->name('levels.edit')
        ->middleware('can:level.edit');

     Route::put('levels/{level}/', 'LevelController@update')
        ->name('levels.update')
        ->middleware('can:levels.edit');

    //class
    Route::get('classe', 'ClassController@index')
        ->name('classe.index')
        ->middleware('can:classe.index');

    Route::get('classe/create', 'ClassController@create')
        ->name('classe.create')
        ->middleware('can:classe.create');

    Route::post('classe/store', 'ClassController@store')
        ->name('classe.store')
        ->middleware('can:classe.create');

    Route::get('classe/{classe}/edit', 'ClassController@edit')
        ->name('classe.edit')
        ->middleware('can:classe.edit');

     Route::put('classe/{classe}/', 'ClassController@update')
        ->name('classe.update')
        ->middleware('can:classe.edit');

    //*Reports*/
    Route::get('reports', 'ReportController@index')
        ->name('reports.index')
        ->middleware('can:reports.index');

    Route::get('reports/create', 'ReportController@create')
        ->name('reports.create')
        ->middleware('can:reports.create');

    Route::post('reports/store', 'ReportController@store')
        ->name('reports.store')
        ->middleware('can:reports.create');

    Route::get('reports/{report}/edit', 'ReportController@edit')
        ->name('reports.edit')
        ->middleware('can:reports.edit');

    Route::put('reports/{report}/', 'ReportController@update')
        ->name('reports.update')
        ->middleware('can:reports.edit');

         //*Schedule*/
    Route::get('schedules', 'ScheduleController@index')
        ->name('schedules.index')
        ->middleware('can:schedules.index');

    Route::get('schedules/create', 'ScheduleController@create')
        ->name('schedules.create')
        ->middleware('can:schedules.create');

    Route::post('schedules/store', 'ScheduleController@store')
        ->name('schedules.store')
        ->middleware('can:schedules.create');

    Route::get('schedules/{schedule}/edit', 'ScheduleController@edit')
        ->name('schedules.edit')
        ->middleware('can:schedules.edit');

    Route::put('schedules/{schedule}/', 'ScheduleController@update')
        ->name('schedules.update')
        ->middleware('can:schedules.edit');

    //*CalendarWeek*/
    Route::post('calendarweek/list/byDate', 'CalendarWeekController@weekByDate')
        ->name('calendarweek.list')
      ->middleware('can:calendarweek.list');

    Route::get('calendarweek', 'CalendarWeekController@index')
        ->name('calendarweek.index')
        ->middleware('can:calendarweek.index');

    Route::get('calendarweek/create', 'CalendarWeekController@create')
        ->name('calendarweek.create')
        ->middleware('can:calendarweek.create');

    Route::post('calendarweek/store', 'CalendarWeekController@store')
        ->name('calendarweek.store')
        ->middleware('can:calendarweek.create');

    Route::get('calendarweek/{calendarweek}/edit', 'CalendarWeekController@edit')
        ->name('calendarweek.edit')
        ->middleware('can:calendarweek.edit');

    Route::put('calendarweek/{calendarweek}/', 'CalendarWeekController@update')
        ->name('calendarweek.update')
        ->middleware('can:calendarweek.update');



    Route::post('catweekdays/getByDate', 'CatWeekdayController@catWeekdaysByDate')
        ->name('catweekdays.getByDate')
       ->middleware('can:catweekdays.getByDate');

    Route::get('catweekdays', 'CatWeekdayController@index')
        ->name('catweekdays.index')
     ->middleware('can:catweekdays.index');

    Route::get('catweekdays/{cat}/edit', 'CatWeekdayController@edit')
        ->name('catweekdays.edit')
     ->middleware('can:catweekdays.edit');

    Route::put('catweekdays/{cat}/', 'CatweekdayController@update')
        ->name('catweekdays.update')
        ->middleware('can:catweekdays.update');

});
